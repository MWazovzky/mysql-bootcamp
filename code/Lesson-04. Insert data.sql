# Insert data
INSERT INTO users (id, name, email)
VALUES (1, 'John', 'john@example.com');

SELECT * FROM users;

# Bulk insert
INSERT INTO users (id, name, email)
VALUES 
    (2, 'Jane', 'jane@example.com'),
    (3, 'Bill', 'bill@example.com'),
    (4, 'Mike', 'mike@example.com')
;

# Warnings
SELECT @@global.sql_mode
# default settings:
# ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION

SET @@global.sql_mode= '';
# turn off strict mode, 
# values that are too long to fit the field are trancated, warning is created
# values that have wrong type are converted if possible or set to type specific default value 

INSERT INTO users (id, name, email)
VALUES (1, 'John', 'some very long string that is way longer then specified field width which is only fifty characters');
# Query OK, 1 row affected, 1 warning (0.00 sec)
SHOW WARNINGS;
/**
+---------+------+--------------------------------------------+
| Level   | Code | Message                                    |
+---------+------+--------------------------------------------+
| Warning | 1265 | Data truncated for column 'email' at row 1 |
+---------+------+--------------------------------------------+
1 row in set (0.00 sec)
*/


INSERT INTO users (id, name, email)
VALUES ('Wrong value type', 'John', 'john@example.com');
# Query OK, 1 row affected, 1 warning (0.00 sec)
SHOW WARNINGS
/**
+---------+------+----------------------------------------------------------------------+
| Level   | Code | Message                                                              |
+---------+------+----------------------------------------------------------------------+
| Warning | 1366 | Incorrect integer value: 'Wrong value type' for column 'id' at row 1 |
+---------+------+----------------------------------------------------------------------+
1 row in set (0.00 sec)
*/

# Constrain NOT NULL
CREATE TABLE users (
    id INT NOT NULL,
    name VARCHAR(50) NOT NULL
);

INSERT INTO users (id)
VALUES (123);

SHOW WARNING:
/**
+---------+------+-------------------------------------------+
| Level   | Code | Message                                   |
+---------+------+-------------------------------------------+
| Warning | 1364 | Field 'name' doesn't have a default value |
+---------+------+-------------------------------------------+
1 row in set (0.00 sec)
*/

SELECT * FROM users;
/*
+-----+------+
| id  | name |
+-----+------+
| 123 |      |
+-----+------+
1 row in set (0.00 sec)
*/
# name is not NULL, it's set to empty string which a default fallback value for VARCHAR type

# DEFAULT VALUE
CREATE TABLE users (
    id INT DEFAULT 123,
    name VARCHAR(50) DEFAULT 'unnamed'
);

DESC users;
/**
+-------+-------------+------+-----+---------+-------+
| Field | Type        | Null | Key | Default | Extra |
+-------+-------------+------+-----+---------+-------+
| id    | int(11)     | YES  |     | 123     |       |
| name  | varchar(50) | YES  |     | unnamed |       |
+-------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)
*/

INSERT INTO users () VALUES();

SELECT * FROM users;
/**
+------+---------+
| id   | name    |
+------+---------+
|  123 | unnamed |
+------+---------+
1 row in set (0.05 sec)
*/

# Aply both NOT NULL and DEFAULT
CREATE TABLE users (
    id INT NOT NULL DEFAULT 123,
    name VARCHAR(50) NOT NULL DEFAULT 'unnamed'
);

INSERT INTO users (id, name) 
VALUES (222, NULL);
# ERROR 1048 (23000): Column 'name' cannot be null

# Primary keys is a unique identifier for a piece of data
CREATE TABLE users (
    id INT NOT NULL,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);
DESC users;
/**
+-------+-------------+------+-----+---------+-------+
| Field | Type        | Null | Key | Default | Extra |
+-------+-------------+------+-----+---------+-------+
| id    | int(11)     | NO   | PRI | NULL    |       |
| name  | varchar(50) | NO   |     | NULL    |       |
+-------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)
*/

# Alternative syntax
CREATE TABLE users (
    id INT NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);
# Any field (or combination) can be used as a primary key

# Primary key may be set automatically
CREATE TABLE users (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL
);

# Exercise
CREATE TABLE employees (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    last_name VARCHAR(255) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    middle_name VARCHAR(255),
    age INT NOT NULL,
    current_status VARCHAR(255) NOT NULL DEFAULT 'employed'
);

DESCRIBE employees;
/**
+----------------+-------------+------+-----+----------+----------------+
| Field          | Type        | Null | Key | Default  | Extra          |
+----------------+-------------+------+-----+----------+----------------+
| id             | int(11)     | NO   | PRI | NULL     | auto_increment |
| last_name      | varchar(255) | NO   |     | NULL     |                |
| first_name     | varchar(255) | NO   |     | NULL     |                |
| middle_name    | varchar(255) | YES  |     | NULL     |                |
| age            | int(11)     | NO   |     | NULL     |                |
| current_status | varchar(255) | NO   |     | employed |                |
+----------------+-------------+------+-----+----------+----------------+
6 rows in set (0.00 sec)
*/

INSERT INTO employees (last_name, first_name, middle_name, age)
VALUES 
    ('Dow', 'John', 'William', 42),
    ('Dow', 'Jane', NULL, 33),
    ('Wazovzky', 'Mike', NULL, 8)
;
/**
Query OK, 3 rows affected (0.00 sec)
Records: 3  Duplicates: 0  Warnings: 0
*/

SELECT * FROM employees;
/**
+----+-----------+------------+-------------+-----+----------------+
| id | last_name | first_name | middle_name | age | current_status |
+----+-----------+------------+-------------+-----+----------------+
|  1 | Dow       | John       | William     |  42 | employed       |
|  2 | Dow       | Jane       | NULL        |  33 | employed       |
|  3 | Wazovzky  | Mike       | NULL        |   8 | employed       |
+----+-----------+------------+-------------+-----+----------------+
3 rows in set (0.02 sec)
*/
