-- Refining Selections

-- DISTINCT
SELECT DISTINCT author_last_name FROM books;
-- works with nmbers
SELECT DISTINCT released_year FROM books;
-- works with multiple columns, rows are distinct if one field is distinct
SELECT DISTINCT CONCAT(author_first_name, ' ', author_last_name) FROM books;
SELECT DISTINCT author_first_name, author_last_name FROM books;

-- ORDER BY
SELECT title, author_last_name, author_first_name 
FROM books 
ORDER BY author_last_name;

SELECT title, author_last_name, author_first_name, stock_quantity 
FROM books 
ORDER BY stock_quantity;

SELECT title, author_last_name, author_first_name, stock_quantity 
FROM books 
ORDER BY stock_quantity DESC;

SELECT title, author_last_name, author_first_name, released_year 
FROM books 
ORDER BY released_year;

-- Order by column, column index (starting from 1) should be specified, it's just an alias to column name
SELECT title, author_last_name, author_first_name, released_year 
FROM books 
ORDER BY 2;

-- Order by multiple columns
SELECT title, author_last_name, author_first_name 
FROM books 
ORDER BY author_last_name, author_first_name;

-- LIMIT
SELECT title, pages 
FROM books 
ORDER BY pages DESC 
LIMIT 5; 

SELECT title, released_year
FROM books 
ORDER BY released_year 
LIMIT 10; 

-- Allows to select starting row, useful for pagination
SELECT title, released_year
FROM books 
ORDER BY released_year 
LIMIT 0, 5; 

SELECT title, released_year
FROM books 
ORDER BY released_year 
LIMIT 5, 5; 

-- same result with OFFSET
SELECT title, released_year
FROM books 
ORDER BY released_year
LIMIT 5
OFFSET 5;

-- can not use OFFSET without LIMIT, use this hack to mimic offset
SELECT title, released_year
FROM books 
LIMIT 777777777777
OFFSET 5;

-- LIKE 
-- patterns matching uses syntax
--   wildcard symbols:
--      '_' - any character
--      '%' - any number of characters
--   contains: %da%
--   starts with: da%
--   ends with: da%
-- Case insentive
SELECT title, author_first_name
FROM books 
WHERE author_first_name LIKE '%da%';

SELECT title, author_first_name
FROM books 
WHERE author_first_name LIKE 'Da%';

SELECT title, author_first_name
FROM books 
WHERE title LIKE '%the%';

SELECT title, author_last_name, author_first_name
FROM books
WHERE author_first_name LIKE '____';

-- Works fine with numeric data
SELECT title, stock_quantity
FROM books
WHERE stock_quantity LIKE '____';

SELECT title, stock_quantity
FROM books
WHERE stock_quantity LIKE '__4';

-- Escaping wildcard characters
SELECT title
FROM books
WHERE title LIKE '%\%%';
/*
+-------------+
| title       |
+-------------+
| 10% Happier |
+-------------+
1 row in set (0.02 sec)
*/

SELECT title
FROM books
WHERE title LIKE '%\_%';
/*
+-----------+
| title     |
+-----------+
| fake_book |
+-----------+
1 row in set (0.02 sec)
*/

-- Exersices
SELECT title 
FROM books
WHERE title LIKE '%stories%';

SELECT title, pages 
FROM books
ORDER BY pages DESC
LIMIT 1;

SELECT CONCAT(title, ' - ', released_year) AS summary
FROM books
ORDER BY released_year DESC
LIMIT 3;

SELECT title, author_last_name
FROM books
WHERE author_last_name LIKE '% %';

SELECT title, released_year, stock_quantity
FROM books
ORDER BY stock_quantity 
LIMIT 3;

SELECT title, author_last_name 
FROM books
ORDER BY author_last_name, title;

SELECT 
    CONCAT(
        'MY FAVORITE AUTHOR IS ', 
        UPPER(author_first_name), 
        ' ', 
        UPPER(author_last_name),
        '!'
    ) AS yell
FROM books
ORDER BY author_last_name;
