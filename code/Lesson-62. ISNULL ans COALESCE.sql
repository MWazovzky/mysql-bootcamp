/**
COALESCE(value,...)
Returns the first non-NULL value in the list, or NULL if there are no non-NULL values.
The return type of COALESCE() is the aggregated type of the argument types.
*/

/**
TODO ...
*/

CREATE VIEW country_view AS
SELECT Code, Name
FROM country;

SELECT *
FROM country_view
LIMIT 5;

CREATE OR REPLACE VIEW country_view AS
SELECT Code, Name, Population
FROM country;

DROP VIEW country_view;

DROP VIEW IF EXISTS country_view;