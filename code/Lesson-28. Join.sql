-- EXCERCISES
SELECT Language, Percentage
FROM countrylanguage
INNER JOIN country ON country.Code = countrylanguage.CountryCode
WHERE Name = 'Russian Federation'
ORDER BY Percentage DESC;

