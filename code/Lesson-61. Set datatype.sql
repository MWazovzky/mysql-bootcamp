CREATE TABLE payments (
    id SERIAL PRIMARY KEY,
    types SET('cash', 'card', 'credit', 'applepay')
);

INSERT INTO payments (types) VALUES
('cash'),
('card,applepay'),
(NULL), 
('card,credit,applepay');

SELECT * FROM payments;

DROP TABLE payments;