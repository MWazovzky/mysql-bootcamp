-- Temporary table
-- usually temporary tables are used as arrays
USE app_node_db;

CREATE TEMPORARY TABLE tmp (
    id INT,
    param VARCHAR(100) DEFAULT 'default'
);

DESC tmp;
/**
+-------+--------------+------+-----+---------+-------+
| Field | Type         | Null | Key | Default | Extra |
+-------+--------------+------+-----+---------+-------+
| id    | int(11)      | YES  |     | NULL    |       |
| param | varchar(100) | YES  |     | default |       |
+-------+--------------+------+-----+---------+-------+
2 rows in set (0.00 sec)
*/

connect;
/**
Connection id:    81
Current database: app_node_db
*/

DESC tmp;
/**
ERROR 1146 (42S02): Table 'app_node_db.tmp' doesn't exist
*/

CREATE TEMPORARY TABLE users2
SELECT * 
FROM users 
WHERE first_name = 'John' AND last_name LIKE 'z%';

SHOW TABLES;
/**
mysql> SHOW TABLES;
+-----------------------+
| Tables_in_app_node_db |
+-----------------------+
| random_numbers        |
| users                 |
+-----------------------+
2 rows in set (0.00 sec)
*/

SELECT * 
FROM users 
WHERE id IN (
    SELECT id FROM users2
);

DROP TEMPORARY TABLE users2;
