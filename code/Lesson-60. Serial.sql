/**
SERIAL is an alias for 
BIGINT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE
*/

CREATE TABLE people (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100)
);

INSERT INTO people (name) VALUES
('John'), ('Bill'), ('Mike');

SELECT * FROM people;

DROP TABLE people;