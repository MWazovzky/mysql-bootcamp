-- Aggregate functions

-- COUNT
SELECT COUNT(*) FROM books;
SELECT COUNT(DISTINCT author_last_name) FROM books;
SELECT COUNT(DISTINCT author_last_name, author_first_name) FROM books;
SELECT COUNT(title) FROM books WHERE title LIKE '%the%';

-- GROUP BY into single row
SELECT author_last_name, COUNT(*) AS books_count 
FROM books 
GROUP BY author_last_name;

-- GROUP BY multiple columns
SELECT author_first_name, author_last_name, COUNT(*) AS books_count 
FROM books 
GROUP BY author_first_name, author_last_name;

SELECT released_year, COUNT(*) AS books_count
FROM books
GROUP BY released_year
ORDER BY books_count DESC;

SELECT CONCAT('In ', released_year, ' ', COUNT(*), ' book(s) released') AS books_per_year
FROM books
GROUP BY released_year
ORDER BY released_year DESC;

-- MIN, MAX, SUM, AVG
SELECT MIN(released_year) FROM books;

SELECT MAX(pages) FROM books;

SELECT author_first_name, author_last_name, SUM(pages) AS 'sum_pages' 
FROM books 
GROUP BY author_first_name, author_last_name 
ORDER BY sum_pages DESC;

SELECT COUNT(*), SUM(pages) FROM books WHERE author_last_name = 'Eggers';

SELECT 
    author_first_name, 
    author_last_name, 
    MAX(pages) AS max_pages, 
    MIN(pages) AS min_pages,
    SUM(pages) AS sum_pages,
    AVG(pages) AS avg_pages
FROM books 
GROUP BY author_first_name, author_last_name
ORDER BY max_pages DESC;

-- SUBQUERY
-- subqueries are slow, runs to queries
SELECT title, pages 
FROM books 
WHERE pages = (SELECT MAX(pages) FROM books);

-- Find the year author published his first book
SELECT author_first_name, author_last_name, MIN(released_year) AS first_book_year
FROM books 
GROUP BY author_first_name, author_last_name
ORDER BY first_book_year;

-- Find the longest book (number of pages) for each author
SELECT author_first_name, author_last_name, MAX(pages) AS max_pages
FROM books 
GROUP BY author_first_name, author_last_name
ORDER BY max_pages DESC;

-- SUM
SELECT SUM(pages) FROM books;

SELECT SUM(stock_quantity) FROM books;

SELECT author_first_name, author_last_name, SUM(pages) AS sum_pages
FROM books 
GROUP BY author_first_name, author_last_name
ORDER BY sum_pages DESC;

-- AVG
SELECT AVG(pages) FROM books;

SELECT author_first_name, author_last_name, ROUND(AVG(pages), 0) AS avg_pages
FROM books 
GROUP BY author_first_name, author_last_name
ORDER BY avg_pages DESC;

SELECT released_year, ROUND(AVG(stock_quantity), 0)
FROM books 
GROUP BY released_year
ORDER BY released_year DESC;

-- Excercises
SELECT COUNT(*) FROM books;
/*
+----------+
| COUNT(*) |
+----------+
|       19 |
+----------+
1 row in set (0.02 sec)
*/

SELECT released_year, COUNT(*) FROM books GROUP BY released_year;
/*
+---------------+----------+
| released_year | COUNT(*) |
+---------------+----------+
|          1945 |        1 |
|          1981 |        1 |
|          1985 |        1 |
|          1989 |        1 |
|          1996 |        1 |
|          2000 |        1 |
|          2001 |        3 |
|          2003 |        2 |
|          2004 |        1 |
|          2005 |        1 |
|          2010 |        1 |
|          2012 |        1 |
|          2013 |        1 |
|          2014 |        1 |
|          2016 |        1 |
|          2017 |        1 |
+---------------+----------+
16 rows in set (0.02 sec)
*/

SELECT SUM(stock_quantity) FROM books;
/*
+---------------------+
| SUM(stock_quantity) |
+---------------------+
|                2450 |
+---------------------+
1 row in set (0.02 sec)
*/

SELECT author_first_name, author_last_name, ROUND(AVG(released_year), 0) 
FROM books
GROUP BY author_first_name, author_last_name
ORDER BY author_last_name;
/*
+-------------------+------------------+------------------------------+
| author_first_name | author_last_name | ROUND(AVG(released_year), 0) |
+-------------------+------------------+------------------------------+
| Raymond           | Carver           |                         1985 |
| Michael           | Chabon           |                         2000 |
| Don               | DeLillo          |                         1985 |
| Dave              | Eggers           |                         2009 |
| David             | Foster Wallace   |                         2005 |
| Neil              | Gaiman           |                         2007 |
| Freida            | Harris           |                         2001 |
| Dan               | Harris           |                         2014 |
| Jhumpa            | Lahiri           |                         2000 |
| George            | Saunders         |                         2017 |
| Patti             | Smith            |                         2010 |
| John              | Steinbeck        |                         1945 |
+-------------------+------------------+------------------------------+
12 rows in set (0.00 sec)
*/

SELECT CONCAT(author_last_name, ', ', author_first_name), title, pages
FROM books
WHERE pages = (SELECT MAX(pages) FROM books);

SELECT CONCAT(author_last_name, ', ', author_first_name), title, pages
FROM books
ORDER BY pages DESC
LIMIT 1;
/*
+---------------------------------------------------+-------------------------------------------+-------+
| CONCAT(author_last_name, ', ', author_first_name) | title                                     | pages |
+---------------------------------------------------+-------------------------------------------+-------+
| Chabon, Michael                                   | The Amazing Adventures of Kavalier & Clay |   634 |
+---------------------------------------------------+-------------------------------------------+-------+
1 row in set (0.00 sec)
*/

SELECT released_year AS 'year', COUNT(*) AS '# books', AVG(pages) AS 'avg pages' 
FROM books
GROUP BY released_year
ORDER BY released_year;
/*
+------+---------+-----------+
| year | # books | avg pages |
+------+---------+-----------+
| 1945 |       1 |  181.0000 |
| 1981 |       1 |  176.0000 |
| 1985 |       1 |  320.0000 |
| 1989 |       1 |  526.0000 |
| 1996 |       1 |  198.0000 |
| 2000 |       1 |  634.0000 |
| 2001 |       3 |  443.3333 |
| 2003 |       2 |  249.5000 |
| 2004 |       1 |  329.0000 |
| 2005 |       1 |  343.0000 |
| 2010 |       1 |  304.0000 |
| 2012 |       1 |  352.0000 |
| 2013 |       1 |  504.0000 |
| 2014 |       1 |  256.0000 |
| 2016 |       1 |  304.0000 |
| 2017 |       1 |  367.0000 |
+------+---------+-----------+
16 rows in set (0.00 sec)
*/
