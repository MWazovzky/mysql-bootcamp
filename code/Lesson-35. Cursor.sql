-- CURSOR
DROP PROCEDURE IF EXISTS sp_cursor_demo;

DELIMITER $$

CREATE PROCEDURE sp_get_largest_countries(IN treshold INT(11))
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE c CHAR(3);
    DECLARE p INT(11);
    DECLARE cur CURSOR FOR SELECT Code, Population FROM country;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

    OPEN cur;

    CREATE TEMPORARY TABLE country_largest (
        code CHAR(3),
        population INT(11)
    );

    REPEAT 
        FETCH cur INTO c, p;
        IF P > treshold 
            THEN INSERT INTO country_largest (code, population) VALUES (c, p);
        END IF;
    UNTIL done END REPEAT;

    CLOSE cur;

    SELECT * FROM country_largest ORDER BY population DESC;

    DROP TABLE IF EXISTS country_largest;
END $$

DELIMITER ;

CALL sp_get_largest_countries(1e8);

DROP PROCEDURE IF EXISTS sp_get_largest_countries;

-- EXAMPLE 2
DROP PROCEDURE IF EXISTS sp_group_countries_by_size;

DELIMITER $$

CREATE PROCEDURE sp_group_countries_by_size(IN tresholdSmall INT(11), IN tresholdLarge INT(11))
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE c VARCHAR(10) DEFAULT 'middle';
    DECLARE n CHAR(52);
    DECLARE p INT(11);
    DECLARE cur CURSOR FOR SELECT Name, Population FROM country;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

    DROP TABLE IF EXISTS countries_classified;
    CREATE TEMPORARY TABLE countries_classified (
        name CHAR(52),
        population INT(11),
        category ENUM('small', 'middle', 'large')
    );

    OPEN cur;

    REPEAT 
        FETCH cur INTO n, p;

        IF P < tresholdSmall 
            THEN SET c = 'small'; 
            ELSE 
                IF P > tresholdLarge
                    THEN SET c = 'large'; 
                    ELSE SET c = 'middle';
                END IF;
        END IF;

        INSERT INTO countries_classified (name, population, category) VALUES (n, p, c);
    UNTIL done END REPEAT;

    CLOSE cur;

    SELECT * 
    FROM countries_classified 
    ORDER BY population DESC;

    SELECT category, COUNT(*) AS count
    FROM countries_classified 
    GROUP BY category
    ORDER BY count DESC;

    DROP TABLE IF EXISTS countries_classified;
END $$

DELIMITER ;

CALL sp_group_countries_by_size(1e6, 1e8);

DROP PROCEDURE IF EXISTS sp_group_countries_by_size;