-- Instagram Clone
 source photogram_data.sql

-- CHALLENGE 1. Find 5 oldest users
SELECT username, created_at 
FROM users
ORDER BY created_at 
LIMIT 5;

-- CHALLENGE 2. What day of the week most users register
SELECT DAYNAME(created_at) AS day_of_week, COUNT(*) AS count 
FROM users
GROUP BY day_of_week
ORDER BY count DESC;
LIMIT 1;

-- CHALLENGE 3. Users who have never posted a photo
SELECT 
    users.username, 
    COUNT(photos.id) AS photos_count
FROM users
LEFT JOIN photos ON users.id = photos.user_id
GROUP BY users.id
HAVING photos_count = 0;

SELECT username 
FROM users
LEFT JOIN photos ON users.id = photos.user_id
WHERE photos.id IS NULL;

-- CHALLENGE 4. Fing users who posted most liked photos
SELECT 
    users.username, 
    photos.image_url, 
    COUNT(*) AS likescount
FROM photos
INNER JOIN likes ON  likes.photo_id = photos.id 
INNER JOIN users ON users.id = photos.user_id
GROUP BY photos.id
ORDER BY likescount DESC
LIMIT 10;

-- alternative solution using subqueries
SELECT username
FROM users
WHERE id = (
    SELECT users.id 
    FROM photos
    INNER JOIN likes ON  likes.photo_id = photos.id 
    INNER JOIN users ON users.id = photos.user_id
    GROUP BY photos.id
    ORDER BY COUNT(*) DESC
    LIMIT 1
);

-- CHALLENGE 5. How many times does the average user posts photos
SELECT AVG(subquery.count) as avg
FROM (
    SELECT COUNT(photos.id) as count
    FROM users
    LEFT JOIN photos ON photos.user_id = users.id
    GROUP BY users.id
) AS subquery;

SELECT (SELECT COUNT(*) FROM photos) / (SELECT COUNT(*) FROM users) AS avg;

-- CHALLENGE 6. Top 5 most popular hashtags
SELECT tags.id, tags.tag_name, COUNT(*) AS tag_count
FROM tags 
JOIN photo_tag ON tags.id = photo_tag.tag_id
GROUP BY tags.id
ORDER BY tag_count DESC
LIMIT 5;

-- CHALLENGE 7. Find users who liked every single photo (bots)
SELECT username, COUNT(*) AS likes_count
FROM users
INNER JOIN likes ON likes.user_id = users.id
GROUP BY users.id
HAVING likes_count = (
    SELECT COUNT(*) FROM photos
)
ORDER BY username;
