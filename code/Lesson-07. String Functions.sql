-- String functions

-- Load bookstore data from file
source books.sql
/. books.sql

-- docs: https://dev.mysql.com/doc/refman/8.0/en/string-functions.html

-- CONCAT()
SELECT title, CONCAT(author_first_name, ' ' , author_last_name) AS author_name FROM books;

SELECT 
    author_first_name AS first_name, 
    author_last_name AS last_name, 
    CONCAT(author_last_name, ', ', author_first_name) AS full_name 
FROM books;

-- CONCAT_WS(), concatatination with separator
SELECT title, CONCAT_WS(' - ', author_first_name, author_last_name) AS author_name FROM books;

-- SUBSTRING(string, start from character index, length)
-- SUBSTRING(string, start from character index)
-- SUBSTR(string, index, length) 
SELECT SUBSTRING('Hello World', 1, 5); -- Hello

SELECT SUBSTRING('Hello World', 7); -- World

SELECT SUBSTRING('Hello World', -5); -- World

SELECT SUBSTRING(title, 1, 10) AS 'short_title' FROM books; 

SELECT CONCAT(SUBSTRING(title, 1, 10), '...') AS 'short_title' FROM books; 

-- REPLACE(string, searrch, replace)
-- case sensitive
SELECT REPLACE('Hello World!!!', 'World', 'Galaxy');

SELECT REPLACE('Hello World!!!', 'l', '7');

SELECT REPLACE(title, ' ', '-') FROM books;

-- REVERSE
SELECT REVERSE('Hello World!'); -- !dlroW olleH

SELECT REVERSE(title) FROM books;

-- CHAR LENGTH
SELECT CHAR_LENGTH('Hello World!');

SELECT title, CHAR_LENGTH(title) AS 'length' FROM books;

-- UPPER(string)
-- LOWER(string)
SELECT UPPER('Hello World!');

SELECT LOWER('Hello World!');

SELECT UPPER(title) AS 'upper', LOWER(title) AS 'lower' FROM books;

-- Exercises
SELECT REVERSE(UPPER('Why does my dog look at me?'));

-- I-like-cats
SELECT REPLACE(CONCAT('I', ' ', 'like', ' ', 'cats'), ' ', '-');

SELECT REPLACE(title, ' ', '->') AS 'title' FROM books;

SELECT 
    author_last_name AS forwards, 
    REVERSE(author_last_name) AS backwards
FROM books;

SELECT UPPER(CONCAT(author_first_name, ' ', author_last_name)) AS 'full name in caps' 
FROM books; 

SELECT CONCAT(title, ' was released in ', released_year) AS blurb
FROM books;

SELECT title, CHAR_LENGTH(title) AS character_count
FROM books;

SELECT 
    CONCAT(SUBSTRING(title, 1, 10), '...') AS 'short title',
    CONCAT(author_last_name, ', ', author_first_name) AS 'author',
    CONCAT(stock_quantity, ' in stock') AS 'quantity'
FROM books;
