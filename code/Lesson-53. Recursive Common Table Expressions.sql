/**
Recursive Common Table Expression
https://dev.mysql.com/doc/refman/8.0/en/with.html
https://dev.mysql.com/doc/refman/8.0/en/with.html#common-table-expressions-recursive
EXAMPLE: https://www.youtube.com/watch?v=M4O0YQGTxjM

WITH RECURSIVE cte_name (col_name, col_name, col_name) AS
(
    subquery base case
    UNION ALL
    subquery referencing cte_name
)
SELECT ... FROM cte_name ...
*/

-- EXAMPLE. Build a sequence
WITH RECURSIVE cte (n) AS
(
  SELECT 1
  UNION ALL
  SELECT n + 1 FROM cte WHERE n < 5
)
SELECT * FROM cte;

-- EXAMPLE. Handlint hierachial data (tree)
--- Create catalog
CREATE TABLE catalog (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    parent_id INT DEFAULT NULL,
    FOREIGN KEY (parent_id) REFERENCES catalog(id) ON DELETE CASCADE
);

--- Root nodes/Categories
INSERT INTO catalog (name) VALUES
('Appliences'),
('Computers'),
('TV'),
('Audio'),
('Furniture');

--- Children nodes/Subcategories && Products
SET @parent_id = (
    SELECT id AS parent_id FROM catalog
    WHERE name = 'Appliences'
    LIMIT 1
);
INSERT INTO catalog (name, parent_id) VALUES
('Major Kitchen Appliences', (SELECT @parent_id)),
('Small Kitchen Appliences', (SELECT @parent_id));

SET @parent_id = (
    SELECT id AS parent_id FROM catalog
    WHERE name = 'Computers'
    LIMIT 1
);
INSERT INTO catalog (name, parent_id) VALUES
('Desktops', (SELECT @parent_id)),
('Notebooks', (SELECT @parent_id)),
('Servers', (SELECT @parent_id));

SET @parent_id = (
    SELECT id AS parent_id FROM catalog
    WHERE name = 'Major Kitchen Appliences'
    LIMIT 1
);
INSERT INTO catalog (name, parent_id) VALUES
('Refrigirators', (SELECT @parent_id)),
('Dishwashers', (SELECT @parent_id)),
('Microwaves', (SELECT @parent_id)),
('Ice Makers', (SELECT @parent_id));

SET @parent_id = (
    SELECT id AS parent_id FROM catalog
    WHERE name = 'Small Kitchen Appliences'
    LIMIT 1
);
INSERT INTO catalog (name, parent_id) VALUES
('Coffee Machines', (SELECT @parent_id)),
('Mixers', (SELECT @parent_id)),
('Toasters', (SELECT @parent_id)),
('Blenders', (SELECT @parent_id));

SELECT * FROM catalog;

--- Get subtree for specified node
WITH RECURSIVE CatalogTree (id, parent_id, name, depth) AS
(
    SELECT id, parent_id, name, 0 AS depth
    FROM catalog
    WHERE id = 1
    UNION ALL
    SELECT c.id, c.parent_id, c.name, ct.depth + 1
    FROM CatalogTree AS ct
    JOIN catalog AS c ON (c.parent_id = ct.id)
)
SELECT * FROM CatalogTree;
