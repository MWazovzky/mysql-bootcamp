-- Indexing
/**
Index - sort data according to certain criteria
Data search is much faster because of binary (b-tree) search 
Indexes should be build for columns that are used as search criteria
*/

-- SYNTAXES:
CREATE [UNIQUE|FULLTEXT|SPATIAL] 
INDEX index_name 
[index_type]
ON table_name (index_column_name, ...)

/*
UNIQUE combination of keys should be unique
FULLTEXT uses all column fields to index it, unlike standard index which is using only first 32 characters,  
SPACIAL works with geo spacial data

index_column_name: colulmn_name [(length)] [ASC|DESC]
Index can be build on multiple columns, order is important
If we define two columns e.g. name, age there will be created two indexes: 
name-age 
name
The order of columns in index should match the order of columns in search query,
otherwise index will not work
Indexes come with overhead 
-> extra disk space is required
-> overhead for CRUD operations (need to update indexes)

index_type: {BTREE|HASH}
-- HASH is used only for tables with engine MEMORY
*/

-- Table with 2 000 000 records
USE app_node_db;

UPDATE users
SET first_name = 'John', last_name = 'Dow', email = 'john@example.com'
WHERE id = CEILING(RAND() * 2000000); 

SELECT * FROM users
WHERE first_name = 'John' AND last_name = 'Dow';
/**
+--------+------------+-----------+------------------+---------------------+
| id     | first_name | last_name | email            | created_at          |
+--------+------------+-----------+------------------+---------------------+
| 422197 | John       | Dow       | john@example.com | 2019-10-18 06:52:27 |
+--------+------------+-----------+------------------+---------------------+
1 row in set (0.99 sec)
*/

CREATE INDEX ix_name
ON users (first_name, last_name);

SELECT * FROM users
WHERE first_name = 'John' AND last_name = 'Dow';
/**
+--------+------------+-----------+------------------+---------------------+
| id     | first_name | last_name | email            | created_at          |
+--------+------------+-----------+------------------+---------------------+
| 422197 | John       | Dow       | john@example.com | 2019-10-18 06:52:27 |
+--------+------------+-----------+------------------+---------------------+
1 row in set (0.04 sec)
*/

DROP INDEX ix_name ON users;

SELECT * FROM users
WHERE first_name = 'John' AND last_name = 'Dow';
/**
+--------+------------+-----------+------------------+---------------------+
| id     | first_name | last_name | email            | created_at          |
+--------+------------+-----------+------------------+---------------------+
| 422197 | John       | Dow       | john@example.com | 2019-10-18 06:52:27 |
+--------+------------+-----------+------------------+---------------------+
1 row in set (0.94 sec)
*/
