/**
Grouping
https://dev.mysql.com/doc/refman/8.0/en/miscellaneous-functions.html#function_grouping

Each argument to GROUPING() must be an expression that exactly matches an expression in the GROUP BY clause. 
For each expression, GROUPING() produces 1 if the expression value in the current row is a NULL 
representing a super-aggregate value. Otherwise, GROUPING() produces 0, indicating that the 
expression value is a NULL for a regular result row or is not NULL.
The expression cannot be a positional specifier. 
Function is permitted only in the select list or HAVING clause.
*/

SELECT 
    country, 
    product, 
    SUM(amount) as 'sum', 
    CAST(AVG(amount) AS DECIMAL(7)) AS 'avg'
FROM sales
GROUP BY country, product WITH ROLLUP;
/**
+---------+-----------+-------+------+
| country | product   | sum   | avg  |
+---------+-----------+-------+------+
| England | toys      |  3400 | 3400 |
| England | NULL      |  3400 | 3400 |
| Germany | auto      |   250 |  250 |
| Germany | computers |   700 |  700 |
| Germany | NULL      |   950 |  475 |
| Russia  | computers |  1600 |  800 |
| Russia  | mobiles   |  2500 | 2500 |
| Russia  | software  |  7400 | 7400 |
| Russia  | NULL      | 11500 | 2875 |
| NULL    | NULL      | 15850 | 2264 |
+---------+-----------+-------+------+
*/

SELECT 
    IF(GROUPING(country), 'ALL', country) AS country,
    IF(GROUPING(product), 'ALL', product) AS product,
    SUM(amount) as 'sum', 
    CAST(AVG(amount) AS DECIMAL(7)) AS 'avg'
FROM sales
GROUP BY country, product WITH ROLLUP;
/**
+---------+-----------+-------------+
| country | product   | SUM(amount) |
+---------+-----------+-------------+
| England | toys      |        3400 |
| England | ALL       |        3400 |
| Germany | auto      |         250 |
| Germany | computers |         700 |
| Germany | ALL       |         950 |
| Russia  | computers |        1600 |
| Russia  | mobiles   |        2500 |
| Russia  | software  |        7400 |
| Russia  | ALL       |       11500 |
| ALL     | ALL       |       15850 |
+---------+-----------+-------------+
*/
