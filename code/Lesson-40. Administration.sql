-- Check table for errors
CHECK TABLE city;
/**
+------------+-------+----------+----------+
| Table      | Op    | Msg_type | Msg_text |
+------------+-------+----------+----------+
| world.city | check | status   | OK       |
+------------+-------+----------+----------+
1 row in set (0.03 sec)
*/

SELECT Host, User, authentication_string FROM mysql.user;

CREATE USER 'john'@'localhost';
SET PASSWORD FOR 'john'@'localhost' = PASSWORD('123');

CREATE USER 'jane'@'localhost' IDENTIFIED BY 'abc';

SELECT Host, User, authentication_string 
FROM mysql.user 
WHERE authentication_string = PASSWORD('123');
/**
+-----------+------+-------------------------------------------+
| Host      | User | authentication_string                     |
+-----------+------+-------------------------------------------+
| localhost | john | *23AE809DDACAF96AF0FD78ED04B6A265E05AA257 |
+-----------+------+-------------------------------------------+
*/

-- Authenticated user check
SELECT USER();
/**
+----------------+
| USER()         |
+----------------+
| root@localhost |
+----------------+
*/

-- Work with user priveledges
SELECT 
    Host, 
    User, 
    authentication_string,  
    Create_priv AS create_table,
    Drop_priv AS drop_table, 
    Insert_priv AS C, 
    Select_priv AS R, 
    Update_priv AS U, 
    Delete_priv AS D
FROM mysql.user
WHERE User = 'john' OR User = 'jane';

GRANT ALL ON world.* TO 'john'@'localhost';
GRANT SELECT, UPDATE ON world.city TO 'jane'@'localhost';

SHOW GRANTS FOR 'john'@'localhost';
/**
+---------------------------------------------------------+
| Grants for john@localhost                               |
+---------------------------------------------------------+
| GRANT USAGE ON *.* TO 'john'@'localhost'                |
| GRANT ALL PRIVILEGES ON `world`.* TO 'john'@'localhost' |
+---------------------------------------------------------+
*/

SHOW GRANTS FOR 'jane'@'localhost';
/**
+--------------------------------------------------------------+
| Grants for jane@localhost                                    |
+--------------------------------------------------------------+
| GRANT USAGE ON *.* TO 'jane'@'localhost'                     |
| GRANT SELECT, UPDATE ON `world`.`city` TO 'jane'@'localhost' |
+--------------------------------------------------------------+
*/

REVOKE ALL PRIVILEGES ON world.city FROM 'jane'@'localhost';
/**
+------------------------------------------+
| Grants for jane@localhost                |
+------------------------------------------+
| GRANT USAGE ON *.* TO 'jane'@'localhost' |
+------------------------------------------+
*/

-- MYSQL 8 Supports Role Based Access Control (RBAC)
