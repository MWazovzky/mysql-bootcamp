/**
Common Table Expression (CTE)
https://dev.mysql.com/doc/refman/8.0/en/with.html
https://dev.mysql.com/doc/refman/8.0/en/with.html#common-table-expressions
CTE is temporary result set used within a query.
CTEs can be used within functions, stored procedures, triggers and views.
MYSQL optimizer may store CTE in memory if there is sufficient memory.

Benefits of CTE
-- Improves readability,
-- Can run internal query separatly to debug
-- Can be nested
-- Can be used recursively
*/

WITH cte AS (
    SELECT country, SUM(amount) AS 'sum'
    FROM sales
    GROUP BY country
)
SELECT 
    sales.country,
    sales.product, 
    sales.amount,
    cte.sum 
FROM sales
INNER JOIN cte ON sales.country = cte.country
ORDER BY country;

-- Define CTE column names
WITH cte (c, p) AS (
    SELECT DISTINCT country, product
    FROM sales
    ORDER BY country, product
)
SELECT * FROM cte;

-- EXAMPLES

-- Countries with the population above aaverage
WITH cte AS (
    SELECT CAST(AVG(Population) AS DECIMAL(10, 0)) AS average_population 
    FROM country
)
SELECT Name, Population 
FROM country
WHERE Population > (SELECT average_population FROM cte)
ORDER BY Population DESC;

WITH cte AS (
    SELECT CAST(AVG(Population) AS DECIMAL(10, 0)) AS average_population 
    FROM country
)
SELECT Name, Population, average_population
FROM country
CROSS JOIN cte
WHERE Population > cte.average_population
ORDER BY Population DESC;

-- Handling duplicates
--- Create duplicates
ALTER TABLE country
ADD COLUMN created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE country
ADD COLUMN id SERIAL FIRST;

INSERT INTO country (Code, Name) VALUES
('CCC', 'China'),
('III', 'India'),
('UUU', 'United States'),
('BBB', 'Brazil'),
('RRR', 'Russian Federation');

SELECT id, Code, Name, created_at
FROM country 
WHERE Name IN ('China', 'India', 'United States', 'Brazil', 'Russian Federation')
ORDER BY Name, id;

--- Find countries without duplicates
SELECT Name, MIN(id) AS min_id 
FROM country
GROUP BY Name;

--- Remove duplicates
WITH cte AS (
    SELECT Name, MIN(id) AS min_id 
    FROM country
    GROUP BY Name
)
DELETE FROM country
WHERE id NOT IN (SELECT min_id FROM cte);


/**
CTEs can be nested
*/
WITH 
regions_statistics AS (
    SELECT Continent, Region, SUM(Population) AS population, SUM(SurfaceArea) AS surface_area
    FROM country
    GROUP BY Continent, Region
),
continents_statistics AS (
    SELECT Continent, SUM(population) AS population, SUM(surface_area) AS surface_area
    FROM regions_statistics
    GROUP BY Continent
) 
SELECT Continent, population, surface_area
FROM continents_statistics
ORDER BY CAST(Continent AS CHAR);
