/**
LOCK/UNLOCK
All tables accessed inside LOCK block should be locked.
lOCK is performed at session level, is released on reconnect.
LOCK types: READ | WRITE | READ LOCAL | LOW_PRIORITY WRITE
*/

/**
Configure mysql timeout:
interactive_timeout=600
wait_timeout=600
innodb_lock_wait_timeout = 300
*/

/**
$ mysql -uroot -p world
*/
LOCK TABLES country READ, city WRITE;

SELECT SUM(Population) FROM country WHERE Code = 'BLR';
/**
+-----------------+
| SUM(Population) |
+-----------------+
|        12385560 |
+-----------------+
1 row in set (0.00 sec)
*/

UPDATE country SET Population = Population * 1.1 WHERE Code = 'BLR';
/**
ERROR 1099 (HY000): Table 'country' was locked with a READ lock and can't be updated
*/

SELECT SUM(Population) FROM city WHERE CountryCode = 'BLR';
/**
+-----------------+
| SUM(Population) |
+-----------------+
|         4741000 |
+-----------------+
1 row in set (0.00 sec)
*/

UPDATE city SET Population = Population * 1.1 WHERE Name = 'Moscow';
/**
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0
*/

UNLOCK TABLES;

/**
$ mysql -umike -p world
trying to work with tables locked be @root
*/
SELECT SUM(Population) FROM country WHERE Code = 'BLR';
/**
+-----------------+
| SUM(Population) |
+-----------------+
|        12385560 |
+-----------------+
1 row in set (0.00 sec)
*/

UPDATE country SET Population = Population * 1.1 WHERE Code = 'BLR';
/**
Blocked, completes after table is unlocked
*/

SELECT SUM(Population) FROM city WHERE CountryCode = 'BLR';
/**
Blocked, completes after table is unlocked
*/

UPDATE city SET Population = Population * 1.1 WHERE Name = 'Moscow';
/**
Blocked, completes after table is unlocked
*/

/**
Backups normally block tables while operating.
*/

/**
Locking whole table is very unefficient.
We can lock specific records, thus keeping the rest of the table accessible

SELECT GET_LOCK ('mylock', 5)
SELECT RELEASE_LOCK ('mylock', 5)
Works just as semaphor, does not really block operations on record(s)
*/
