# Create & Delete Database
SHOW DATABASES;
CREATE DATABASE practice_sql_db;
DROP DATABASE IF EXISTS practice_sql_db;
USE practice_sql_db;
SELECT database();
# May be NULL if no database is selected

# Create & Delete Table
CREATE TABLE users 
(
    id INT,
    name VARCHAR(100),
    email VARCHAR(50)
);
SHOW TABLES;
SHOW COLUMNS FROM users;
DESCRIBE users;
DROP TABLE IF EXISTS users;
