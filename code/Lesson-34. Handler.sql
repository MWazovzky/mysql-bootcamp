/**
Handler may be user in store procedures and functiions

Handler: https://dev.mysql.com/doc/refman/8.0/en/declare-handler.html
Error: https://dev.mysql.com/doc/refman/8.0/en/error-handling.html

DECLARE handler_type HANDLER FOR condition_value[,...]

handler_type: CONTINUE | EXIT | UNDO
UNDO is not implemented

condition_value: SQLSTATE [VALUE] sqlestate_value | condition_name | SQLWARNINGS  | NOT FOUND | SQLEXCEPTION| mysql_error_code
*/

DROP PROCEDURE IF EXISTS sp_handler_example;
DELIMITER $$

CREATE PROCEDURE sp_handler_example(IN countryName CHAR(52))
BEGIN
    DECLARE countryCode CHAR(3);
    DECLARE CONTINUE HANDLER FOR SQLSTATE '23000' SET @err_state = 23000;

    SELECT Code INTO countryCode 
    FROM country
    WHERE Name = countryName;

    INSERT INTO country (Code, Name) VALUES (countryCode, 'Fake Country');
END $$

DELIMITER ;

SELECT @err_state;
/**
+------------+
| @err_state |
+------------+
| NULL       |
+------------+
1 row in set (0.00 sec)
*/

CALL sp_handler_example('China');
/**
Query OK, 0 rows affected (0.03 sec)
*/

SELECT @err_state;
/**
+------------+
| @err_state |
+------------+
|      23000 |
+------------+
1 row in set (0.00 sec)
*/

DROP PROCEDURE IF EXISTS sp_handler_example;
