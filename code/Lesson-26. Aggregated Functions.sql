-- COUNT
SELECT COUNT(*) AS users_count FROM users;

/**
COUNT(*) caculates number of rows in table
COUNT(column) calculates number of rows where column value NOT NULL
*/

-- GROUP_CONCAT()
SELECT GROUP_CONCAT(name) FROM 
(SELECT DISTINCT first_name AS name FROM users LIMIT 10) AS user_names;

SELECT GROUP_CONCAT(DISTINCT name SEPARATOR ';') AS names_list FROM 
(SELECT DISTINCT first_name AS name FROM users LIMIT 10) AS user_names;

/**
The result is truncated to the maximum length that is given by the group_concat_max_len system variable, 
which has a default value of 1024. 
The value can be set higher, although the effective maximum length of the return value is constrained by 
the value of max_allowed_packet. 
The syntax to change the value of group_concat_max_len at runtime is as follows, where val is an unsigned integer:
SET [GLOBAL | SESSION] group_concat_max_len = val;
The return value is a nonbinary or binary string, depending on whether the arguments are nonbinary or binary strings. 
The result type is TEXT or BLOB unless group_concat_max_len is less than or equal to 512, in which case the result 
type is VARCHAR or VARBINARY.
*/

SET group_concat_max_len = 1000000;

SELECT GROUP_CONCAT(
    DISTINCT first_name 
    ORDER BY first_name DESC
    SEPARATOR ';'
) AS names_list 
FROM users
INTO OUTFILE 'c:/users/dev/ospanel/domains/learning/mysql-bootcamp/users_first_names_list.csv';

-- OTHER AGGREGATION FUNCTION
-- MIN()
-- MAX()
-- SUM()
-- AVG()
-- STD()
-- VARIANCE()

SELECT YEAR(created_at) as year, COUNT(first_name) AS count
FROM users
GROUP BY year;

SELECT MONTHNAME(created_at) as month, COUNT(first_name) AS count
FROM users
GROUP BY month;

/**
Remove ONLY_FULL_GROUP_BY to use non-aggreagate value
*/
SELECT @@global.sql_mode;
SELECT @@session.sql_mode;
SELECT @@sql_mode;
/**
+-------------------------------------------------------------------------------------------------------------------------------------------+
| @@global.sql_mode                                                                                                                         |
+-------------------------------------------------------------------------------------------------------------------------------------------+
| ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION |
+-------------------------------------------------------------------------------------------------------------------------------------------+
1 row in set (0.02 sec)
*/

SET sql_mode = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';

SELECT 
    YEAR(created_at) as year, 
    MONTH(created_at) as month, 
    MONTHNAME(created_at) AS monthname, 
    COUNT(first_name) AS count
FROM users
GROUP BY year, month
ORDER BY year, month;

-- WITH ROLLUP
SELECT 
    YEAR(created_at) as year, 
    MONTH(created_at) as month, 
    MONTHNAME(created_at) AS monthname, 
    COUNT(first_name) AS count
FROM users
GROUP BY year, month WITH ROLLUP;

/**
We can not use WITH ROLLUP and ORDER BY in one query (WAT?!).
Use subquery as work around.
Use IS NULL to have NULL values last in the list.
*/
SELECT year, month, monthname, count FROM (
    SELECT 
        YEAR(created_at) as year, 
        MONTH(created_at) as month, 
        MONTHNAME(created_at) AS monthname, 
        COUNT(first_name) AS count
    FROM users
    GROUP BY year, month WITH ROLLUP
) AS users_aggregated_by_created_at
order by year IS NULL, year, month IS NULL, month;

SELECT AVG(count) AS avg_count FROM (
    SELECT COUNT(first_name) AS count
    FROM users
    GROUP BY first_name
) AS names_count;


-- HAVING
SELECT first_name, COUNT(*) AS count
FROM users
GROUP BY first_name
HAVING count > 740
ORDER BY count DESC;

SELECT first_name, COUNT(*) AS count
FROM users
GROUP BY first_name
HAVING count < 600
ORDER BY count ASC;
