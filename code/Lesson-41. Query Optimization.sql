-- Query Optimzation
USE world;

SHOW INDEX FROM city;

-- Slow query example
SELECT SQL_NO_CACHE COUNT(*) 
FROM city as c1
WHERE EXISTS (
    SELECT * FROM city AS c2 WHERE c1.Name = c2.Name
);
/**
+----------+
| COUNT(*) |
+----------+
|     4075 |
+----------+
1 row in set, 1 warning (2.21 sec)
*/

-- https://dev.mysql.com/doc/refman/8.0/en/explain-output.html
EXPLAIN SELECT SQL_NO_CACHE COUNT(*) 
FROM city as c1
WHERE EXISTS (
    SELECT * FROM city AS c2 WHERE c1.Name = c2.Name
) \G
/**
*************************** 1. row ***************************
           id: 1
  select_type: PRIMARY
        table: c1
   partitions: NULL
         type: ALL
possible_keys: NULL
          key: NULL
      key_len: NULL
          ref: NULL
         rows: 4071
     filtered: 100.00
        Extra: Using where
*************************** 2. row ***************************
           id: 2
  select_type: DEPENDENT SUBQUERY
        table: c2
   partitions: NULL
         type: ALL
possible_keys: NULL
          key: NULL
      key_len: NULL
          ref: NULL
         rows: 4071
     filtered: 10.00
        Extra: Using where
2 rows in set, 3 warnings (0.00 sec)
*/

SHOW INDEX FROM city;
/**
+-------+------------+-------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| Table | Non_unique | Key_name    | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment |
+-------+------------+-------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| city  |          0 | PRIMARY     |            1 | ID          | A         |        4071 |     NULL | NULL   |      | BTREE      |         |               |
| city  |          1 | CountryCode |            1 | CountryCode | A         |         231 |     NULL | NULL   | YES  | BTREE      |         |               |
+-------+------------+-------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
*/

CREATE INDEX ix_name ON city (Name);
/**
+-------+------------+-------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| Table | Non_unique | Key_name    | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment |
+-------+------------+-------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| city  |          0 | PRIMARY     |            1 | ID          | A         |        4071 |     NULL | NULL   |      | BTREE      |         |               |
| city  |          1 | CountryCode |            1 | CountryCode | A         |         231 |     NULL | NULL   | YES  | BTREE      |         |               |
| city  |          1 | ix_name     |            1 | Name        | A         |        3994 |     NULL | NULL   |      | BTREE      |         |               |
+-------+------------+-------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
3 rows in set (0.00 sec)
*/

SELECT SQL_NO_CACHE COUNT(*) 
FROM city as c1
WHERE EXISTS (
    SELECT * FROM city AS c2 WHERE c1.Name = c2.Name
);
/**
+----------+
| COUNT(*) |
+----------+
|     4075 |
+----------+
1 row in set, 1 warning (0.01 sec)
*/

EXPLAIN SELECT SQL_NO_CACHE COUNT(*) 
FROM city as c1
WHERE EXISTS (
    SELECT * FROM city AS c2 WHERE c1.Name = c2.Name
) \G
/**
*************************** 1. row ***************************
           id: 1
  select_type: PRIMARY
        table: c1
   partitions: NULL
         type: index
possible_keys: NULL
          key: ix_name
      key_len: 35
          ref: NULL
         rows: 4071
     filtered: 100.00
        Extra: Using where; Using index
*************************** 2. row ***************************
           id: 2
  select_type: DEPENDENT SUBQUERY
        table: c2
   partitions: NULL
         type: ref
possible_keys: ix_name
          key: ix_name
      key_len: 35
          ref: world.c1.Name
         rows: 1
     filtered: 100.00
        Extra: Using index
2 rows in set, 3 warnings (0.00 sec)
*/
