/**
Procedures are stored inside mysql.
Instead of sending complex query user may call predefined procedure.
https://dev.mysql.com/doc/refman/5.7/en/create-procedure.html

PARAMS
Procedures may have parameters.
Params must have type  
Procedures params must have category: IN, OUT, INOUT. 
Procedures may use variables.

Procedures are stored in database.
Show stored procedures listing:
SHOW PROCEDURE STATUS [LIKE 'pattern' | WHERE search_condition]
*/

USE world;

-- Procedure with arguments
DROP PROCEDURE IF EXISTS sp_find_country;

DELIMITER $$

CREATE PROCEDURE sp_find_country(IN countryCode CHAR(3))
BEGIN
    SELECT Name 
    FROM country
    WHERE Code = countryCode;
END $$

DELIMITER ;

CALL sp_find_country('ARG');
CALL sp_find_country('RUS');

-- Procedure without arguments
DROP PROCEDURE IF EXISTS sp_find_country_rus;
DELIMITER $$

CREATE PROCEDURE sp_find_country_rus()
BEGIN
    SELECT Name 
    FROM country
    WHERE Code = 'RUS';
END $$

DELIMITER ;

/**
SHOW PROCEDURE STATUS [LIKE 'pattern' | WHERE search_condition]
*/
SHOW PROCEDURE STATUS LIKE '%country%';
SHOW PROCEDURE STATUS WHERE Db = 'world';
/**
+-------+-----------------+-----------+---------+---------------------+---------------------+---------------+---------+----------------------+----------------------+--------------------+
| Db    | Name            | Type      | Definer | Modified            | Created             | Security_type | Comment | character_set_client | collation_connection | Database Collation |
+-------+-----------------+-----------+---------+---------------------+---------------------+---------------+---------+----------------------+----------------------+--------------------+
| world | sp_find_country | PROCEDURE | root@%  | 2020-01-08 10:41:07 | 2020-01-08 10:41:07 | DEFINER       |         | utf8                 | utf8_general_ci      | utf8_unicode_ci    |
+-------+-----------------+-----------+---------+---------------------+---------------------+---------------+---------+----------------------+----------------------+--------------------+
1 row in set (0.02 sec)
*/

DROP PROCEDURE IF EXISTS sp_find_country;
DROP PROCEDURE IF EXISTS sp_find_country_rus;

-- VAR
DROP PROCEDURE IF EXISTS sp_countries_above_average;
DELIMITER $$

CREATE PROCEDURE sp_countries_above_average(IN num INTEGER)
BEGIN
    DECLARE queryLimit INTEGER(11) DEFAULT 0;
    DECLARE avgPopulation INTEGER(11) DEFAULT 0;

    SET queryLimit = num;
    
    SELECT AVG(Population) INTO avgPopulation 
    FROM country;

    SELECT Name, Population 
    FROM country
    WHERE Population > avgPopulation
    ORDER BY Population DESC
    LIMIT queryLimit;
END $$

DELIMITER ;

CALL sp_countries_above_average(10);
CALL sp_countries_above_average(100);

DROP PROCEDURE IF EXISTS sp_countries_above_average;
 