-- EVENTS

CREATE TABLE random_numbers (
    num INT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE EVENT generate_random_number
ON SCHEDULE EVERY 20 SECOND
DO
INSERT INTO random_numbers (num) VALUES 
(CEILING(RAND() * 100));

-- Check scheduler status
SHOW VARIABLES LIKE '%schedule%';
/**
+-----------------+-------+
| Variable_name   | Value |
+-----------------+-------+
| event_scheduler | OFF   |
+-----------------+-------+
1 row in set (0.00 sec)
*/

-- Turn scheduler ON
SET GLOBAL event_scheduler = 1;
SHOW VARIABLES LIKE '%schedule%';

SELECT * FROM random_numbers
ORDER BY created_at DESC;

-- Review events
DESC information_schema.events;

SELECT EVENT_NAME, EVENT_TYPE, EXECUTE_AT, INTERVAL_FIELD, INTERVAL_VALUE, STARTS, ENDS, STATUS 
FROM information_schema.events;

SHOW CREATE TABLE random_numbers;
SHOW CREATE EVENT generate_random_number;


/**
To stop event we can:
-> disable specific event
-> drop event
-> stop scheduler - stop all events
*/
ALTER EVENT generate_random_number DISABLE;

DROP EVENT IF EXISTS generate_random_number;
