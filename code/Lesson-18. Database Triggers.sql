/** 
Database triggers
https://dev.mysql.com/doc/refman/5.7/en/trigger-syntax.html
All operations performed by trigger are regarded as one transaction  
*/
USE photogram;

ALTER TABLE users 
ADD COLUMN age INT AFTER username;

INSERT INTO users (username, age) VALUES ('jimmy', 16);
/**
Query OK, 1 row affected (0.00 sec)
*/

DELIMITER $$
CREATE TRIGGER only_adult
    BEFORE INSERT ON users 
    FOR EACH ROW
    BEGIN
        IF NEW.age < 18
        THEN 
            SIGNAL SQLSTATE '45000' 
            SET MESSAGE_TEXT = 'User must be 18+';
        END IF;
    END;
$$
DELIMITER ;

INSERT INTO users (username, age) VALUES ('jane', 13);
/**
ERROR 1644 (45000): User must be 18+
*/

SELECT * FROM users WHERE NOT age IS NULL;
/**
+-----+----------+------+---------------------+
| id  | username | age  | created_at          |
+-----+----------+------+---------------------+
| 101 | jimmy    |   16 | 2019-12-31 10:51:23 |
+-----+----------+------+---------------------+
1 row in set (0.00 sec)
*/

/**
TRIGGER OPTIONS
time: BEFOR | AFTER
event: INSERT | UPDATE | DELETE
table: any existing table

TRIGGER HAS ACCESS TO MODIFIED ROW FIELDS: 
NEW.column, OLD.column

TRIGGER ERROR CONSISTS OF THREE COMPONENTS
https://dev.mysql.com/doc/refman/5.7/en/server-error-reference.html
1. Numeric error code (1644) is set by mysql
2. Five character SQLSTATE (45000). 
Standardized codes taken from ANSI SQL and ODBC
45000 is generic state representing 'unhandled user defined exeption'.
3. Message string
*/

-- Prevent users from following themselves

INSERT INTO follows (follower_id ,followee_id) VALUES (101, 101);

SELECT follower_id, followee_id, username 
FROM follows
JOIN users ON users.id = follows.follower_id 
WHERE follower_id = followee_id;
/**
+-------------+-------------+-----------+
| follower_id | followee_id | username  |
+-------------+-------------+-----------+
|         101 |         101 | jimmy     |
+-------------+-------------+-----------+
1 rows in set (0.02 sec)
*/

DELIMITER $$
CREATE TRIGGER prevent_self_following
    BEFORE INSERT ON follows 
    FOR EACH ROW
    BEGIN
        IF NEW.follower_id = NEW.followee_id 
        THEN 
            SIGNAL SQLSTATE '45000' 
            SET MESSAGE_TEXT = 'User may not follow himself.';
        END IF;
    END
$$
DELIMITER ;

INSERT INTO follows (follower_id ,followee_id) VALUES (100, 100);
/**
ERROR 1644 (45000): User may not follow himself.
*/

INSERT INTO follows (follower_id ,followee_id) VALUES (100, 100);
/**
Query OK, 1 row affected (0.00 sec)
*/

SELECT follower_id, followee_id, username 
FROM follows
JOIN users ON users.id = follows.follower_id 
WHERE follower_id = followee_id;
/**
+-------------+-------------+-----------+
| follower_id | followee_id | username  |
+-------------+-------------+-----------+
|         100 |         100 | Javonte83 |
|         101 |         101 | jimmy     |
+-------------+-------------+-----------+
2 rows in set (0.02 sec)
*/

-- LOG UNFOLLOWS
CREATE TABLE unfollows (
    follower_id INT,
    followee_id INT,
    created_at TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY(follower_id) REFERENCES users(id),
    FOREIGN KEY(followee_id) REFERENCES users(id)
);

SELECT * FROM unfollows;

DELIMITER $$
CREATE TRIGGER log_unfollows
    AFTER DELETE ON follows
    FOR EACH ROW
    BEGIN
        INSERT INTO unfollows 
        SET follower_id = OLD.follower_id, followee_id = OLD.followee_id;
    END
$$
DELIMITER ;

DELETE FROM follows 
WHERE follower_id = followee_id;
/**
Query OK, 2 rows affected (0.03 sec)
*/

SELECT * FROM unfollows;
/**
+-------------+-------------+---------------------+
| follower_id | followee_id | created_at          |
+-------------+-------------+---------------------+
|         100 |         100 | 2019-12-31 12:04:43 |
|         101 |         101 | 2019-12-31 12:04:43 |
+-------------+-------------+---------------------+
2 rows in set (0.02 sec)
*/

SHOW TRIGGERS;

DROP TRIGGER IF EXISTS only_adult;

/**
WARNING! Triggers can make debugging hard! Some magic happens behind the scene...
*/


/** 
EXCERSISE #1
Trigger city.CountryCode update when country.code is changed
*/

USE world;

SELECT CountryCode, COUNT(*) AS count
FROM city
GROUP BY CountryCode
ORDER BY count DESC;

DELIMITER $$

CREATE TRIGGER tg_update_country_code
    AFTER UPDATE ON country
    FOR EACH ROW
    BEGIN
        UPDATE city 
        SET CountryCode = NEW.Code 
        WHERE CountryCode = OLD.Code;
    END
$$
DELIMITER ;

SELECT Name, CountryCode FROM city WHERE CountryCode = 'DOM';
/**
+----------------------------+-------------+
| Name                       | CountryCode |
+----------------------------+-------------+
| Santo Domingo de Guzmán    | DOM         |
| Santiago de los Caballeros | DOM         |
| La Romana                  | DOM         |
| San Pedro de Macorís       | DOM         |
| San Francisco de Macorís   | DOM         |
| San Felipe de Puerto Plata | DOM         |
+----------------------------+-------------+
6 rows in set (0.00 sec)
*/

SET FOREIGN_KEY_CHECKS = 0;

UPDATE country SET Code = 'XXX' WHERE Code = 'DOM';

SET FOREIGN_KEY_CHECKS = 1;

SELECT Name, CountryCode FROM city WHERE CountryCode = 'DOM';
/**
Empty set (0.00 sec)
*/

SELECT Name, CountryCode FROM city WHERE CountryCode = 'XXX';
/**
+----------------------------+-------------+
| Name                       | CountryCode |
+----------------------------+-------------+
| Santo Domingo de Guzmán    | XXX         |
| Santiago de los Caballeros | XXX         |
| La Romana                  | XXX         |
| San Pedro de Macorís       | XXX         |
| San Francisco de Macorís   | XXX         |
| San Felipe de Puerto Plata | XXX         |
+----------------------------+-------------+
6 rows in set (0.02 sec)
*/

DROP TRIGGER IF EXISTS tg_update_country_code;

/** 
EXCERSISE #2
Update cities when country is deleted
*/

-- Get foreign key name
SHOW CREATE TABLE city;

-- Drop foreign key
ALTER TABLE city    
DROP FOREIGN KEY city_ibfk_1;

-- Make field nullable 
ALTER TABLE city    
MODIFY CountryCode CHAR(3) DEFAULT NULL;

DELIMITER $$

CREATE TRIGGER tg_delete_country
    AFTER DELETE ON country
    FOR EACH ROW
    BEGIN
        UPDATE city SET CountryCode = NULL WHERE CountryCode = OLD.Code;
    END
$$

DELIMITER ;

DELETE FROM country WHERE Code = 'XXX';

SELECT Name, CountryCode FROM city WHERE CountryCode IS NULL;
/**
+----------------------------+-------------+
| Name                       | CountryCode |
+----------------------------+-------------+
| Santo Domingo de Guzmán    | NULL        |
| Santiago de los Caballeros | NULL        |
| La Romana                  | NULL        |
| San Pedro de Macorís       | NULL        |
| San Francisco de Macorís   | NULL        |
| San Felipe de Puerto Plata | NULL        |
+----------------------------+-------------+
6 rows in set (0.02 sec)
*/

DROP TRIGGER IF EXISTS tg_delete_country;

/**
Do not try to mofify the table trigger is created on inside trigger body!
Recursive calls and overflow.
*/
  