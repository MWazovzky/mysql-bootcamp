# CRUD: Create, Read, Update, Delete

# Prepair data
CREATE TABLE cats (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100),
    breed VARCHAR(100),
    age INT
);

INSERT INTO cats (name, breed, age) 
VALUES ('Ringo', 'Tabby', 4),
       ('Cindy', 'Maine Coon', 10),
       ('Dumbledore', 'Maine Coon', 11),
       ('Egg', 'Persian', 4),
       ('Misty', 'Tabby', 13),
       ('George Michael', 'Ragdoll', 9),
       ('Jackson', 'Sphynx', 7)
;

# READ, SELECT operator
SELECT * FROM cats;

# SELECT Expression (specified fields)
SELECT name FROM cats;
SELECT name, breed, age FROM cats;

# WHERE clause
SELECT * FROM cats WHERE breed = 'Maine Coon';
SELECT * FROM cats WHERE breed = 'MAINE COON';
# Case insencitive by default
SELECT * FROM cats WHERE breed = 'Maine Coon' OR breed = 'Tabby';
SELECT * FROM cats WHERE age >= 10;

# Exercise SELECT
SELECT id FROM cats;
SELECT name, breed FROM cats;
SELECT name, age FROM cats WHERE breed = 'Tabby';
SELECT id, age, name FROM cats WHERE id = age;

# UPDATE


# Exercise UPDATE
UPDATE cats 
SET name = 'Jack'
WHERE name = 'Jackson';

UPDATE cats 
SET breed = 'British Shorthair'
WHERE name = 'Ringo';

UPDATE cats 
SET age = 12
WHERE breed = 'Maine Coon';

# Exercise DELETE
DELETE FROM cats
WHERE age = 4;

DELETE FROM cats
WHERE age = id;

DELETE FROM cats;
