SHOW ENGINES;
/**
+--------------------+---------+----------------------------------------------------------------+--------------+------+------------+
| Engine             | Support | Comment                                                        | Transactions | XA   | Savepoints |
+--------------------+---------+----------------------------------------------------------------+--------------+------+------------+
| InnoDB             | DEFAULT | Supports transactions, row-level locking, and foreign keys     | YES          | YES  | YES        |
| MRG_MYISAM         | YES     | Collection of identical MyISAM tables                          | NO           | NO   | NO         |
| MEMORY             | YES     | Hash based, stored in memory, useful for temporary tables      | NO           | NO   | NO         |
| BLACKHOLE          | YES     | /dev/null storage engine (anything you write to it disappears) | NO           | NO   | NO         |
| MyISAM             | YES     | MyISAM storage engine                                          | NO           | NO   | NO         |
| CSV                | YES     | CSV storage engine                                             | NO           | NO   | NO         |
| ARCHIVE            | YES     | Archive storage engine                                         | NO           | NO   | NO         |
| PERFORMANCE_SCHEMA | YES     | Performance Schema                                             | NO           | NO   | NO         |
| FEDERATED          | NO      | Federated MySQL storage engine                                 | NULL         | NULL | NULL       |
+--------------------+---------+----------------------------------------------------------------+--------------+------+------------+
9 rows in set (0.02 sec)

Comments:
MyISAM, no transaction, simpler, faster (ispecially for read intensive operaations), takes less disk space (2-3 times less compared to InnoDB), 
MRG_MYISAM, allows to merge multiple MyISAM tables if memory limit achived
MEMORY, blazing fast
ARCHIVE, takes 3 times less space then MyISAM, very slow
CSV, regards .txt file as database

Default ENGINE can be changed via mysql.ini config file.
*/

USE world;

CREATE TABLE tmp
ENGINE = INNODB
AS SELECT * FROM city;
/**
Query OK, 4075 rows affected (0.20 sec)
Records: 4075  Duplicates: 0  Warnings: 0
*/
SHOW CREATE TABLE tmp;

SELECT SQL_NO_CACHE COUNT(*) 
FROM tmp AS t1 
WHERE EXISTS(
    SELECT * FROM tmp AS t2 WHERE t1.name = t2.name
);
/**
+----------+
| COUNT(*) |
+----------+
|     4075 |
+----------+
1 row in set, 1 warning (2.88 sec)
*/

ALTER TABLE tmp 
ENGINE = MYISAM;

SHOW CREATE TABLE tmp;

SELECT SQL_NO_CACHE COUNT(*) 
FROM tmp AS t1 
WHERE EXISTS(
    SELECT * FROM tmp AS t2 WHERE t1.name = t2.name
);
/**
+----------+
| COUNT(*) |
+----------+
|     4075 |
+----------+
1 row in set, 1 warning (15.34 sec)
*/

/**
To store data in memory memory configuration may be required .
*/
SELECT @@max_heap_table_size;
/**
+-----------------------+
| @@max_heap_table_size |
+-----------------------+
|              26214400 |
+-----------------------+
*/

SELECT @@tmp_table_size;
/**
+------------------+
| @@tmp_table_size |
+------------------+
|         26214400 |
+------------------+
*/

SET tmp_table_size = 1024 * 1024 * 1024 * 10;
SET max_heap_table_size = 1024 * 1024 * 1024 * 10;

SELECT @@max_heap_table_size;
/**
+-----------------------+
| @@max_heap_table_size |
+-----------------------+
|           10737418240 |
+-----------------------+
*/

SELECT @@tmp_table_size;
/**
+------------------+
| @@tmp_table_size |
+------------------+
|      10737418240 |
+------------------+
*/

CREATE TABLE tmp_memory 
ENGINE = MEMORY
AS SELECT * FROM city;
/**
Query OK, 4075 rows affected (0.04 sec)
Records: 4075  Duplicates: 0  Warnings: 0
*/

SELECT SQL_NO_CACHE COUNT(*) 
FROM tmp_memory AS t1 
WHERE EXISTS(
    SELECT * FROM tmp_memory AS t2 WHERE t1.name = t2.name
);
/**
+----------+
| COUNT(*) |
+----------+
|     4075 |
+----------+
1 row in set, 1 warning (0.64 sec)
5-x times faster then InnoDB
*/
