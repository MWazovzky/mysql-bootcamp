-- DATA MANIPULATION
-- INSERT
INSERT INTO users (first_name, last_name, email) 
VALUES ('John', 'Dow', 'john@example.com'); 

INSERT INTO users
SET first_name = 'Jane', last_name = 'Dow', email = 'jane@example.com';

INSERT INTO users (first_name, last_name, email)
SELECT first_name, last_name, email FROM book_shop.customers;

-- Check if there is a folder specified for secure file load
SHOW VARIABLES LIKE 'secure_file_priv';
/**
+------------------+-------------------------------------+
| Variable_name    | Value                               |
+------------------+-------------------------------------+
| secure_file_priv | c:\users\dev\ospanel\userdata\temp\ |
+------------------+-------------------------------------+
1 row in set (0.05 sec)
*/
-- It can be disabled via 'secure-file-priv' option at 'mysql.ini' configuration file
-- secure-file-priv = ""
SHOW VARIABLES LIKE 'secure_file_priv';
/**
+------------------+-------+
| Variable_name    | Value |
+------------------+-------+
| secure_file_priv | NULL  |
+------------------+-------+
1 row in set (0.00 sec)
*/

LOAD DATA INFILE 'c:/users/dev/ospanel/domains/learning/mysql-bootcamp/users_data_test.csv'
IGNORE 
INTO TABLE users
FIELDS 
    TERMINATED BY ';'
LINES
    TERMINATED BY '\r\n';

SET NAMES cp866;
SET NAMES utf8;

-- DELETE

TRUNCATE TABLE users;