-- SELECT

-- DISTINCT
SELECT DISTINCT first_name FROM users;

-- SQL_NO_CACHE 

-- ORDER BY N
SELECT DISTINCT first_name 
FROM users
ORDER BY 1 DESC;

-- ORDER BY RAND()
-- very slow
SELECT DISTINCT first_name 
FROM users
ORDER BY RAND()
LIMIT 1;
/**
+------------+
| first_name |
+------------+
| Rosemary   |
+------------+
1 row in set (1.99 sec)
*/

-- Alternative
SET @rand_number = CEILING(RAND() * 2000000);
SELECT id, first_name 
FROM users
WHERE id = @rand_number;
/**
+-------+------------+
| id    | first_name |
+-------+------------+
| 47779 | Tad        |
+-------+------------+
1 row in set (0.03 sec)
*/

-- LIMIT/OFFSET
SELECT id, first_name
FROM users
ORDER BY id
LIMIT 5 OFFSET 10;

-- Skips 10, shows 5, is slower then previous query
SELECT id, first_name
FROM users
ORDER BY id
LIMIT 10, 5;

SELECT id, first_name
FROM users
WHERE first_name LIKE 'Mi%' AND id < 100000 OR id = 2000000;

-- GROUP BY WITH ROLLUP
SELECT first_name, COUNT(*) AS count 
FROM users
GROUP BY first_name WITH ROLLUP;

-- INTO OUTFILE
SELECT first_name, COUNT(*) AS count 
FROM users
GROUP BY first_name
INTO OUTFILE 'c:/users/dev/ospanel/domains/learning/mysql-bootcamp/users_first_names.csv';
