/**
Transactions
A - Atomicity
C - Consistency
I - Isolation
D - Durability

Do not use transaction with tables other then InnoDB
START TRANSACTION
    [transaction_characteristic [, transaction_characteristic] ...]

transaction_characteristic: {
    WITH CONSISTENT SNAPSHOT
  | READ WRITE
  | READ ONLY
}

BEGIN [WORK]
COMMIT [WORK] [AND [NO] CHAIN] [[NO] RELEASE]
ROLLBACK [WORK] [AND [NO] CHAIN] [[NO] RELEASE]
SET autocommit = {0 | 1}

https://dev.mysql.com/doc/refman/8.0/en/commit.html
*/
USE world;

SELECT COUNT(*) FROM city;
/**
+----------+
| COUNT(*) |
+----------+
|     4079 |
+----------+
1 row in set (0.00 sec)
*/

START TRANSACTION;

DELETE FROM city WHERE CountryCode <> 'RUS';

SELECT COUNT(*) FROM city;
/**
+----------+
| COUNT(*) |
+----------+
|      195 |
+----------+
1 row in set (0.00 sec)
*/
ROLLBACK;

SELECT COUNT(*) FROM city;
/**
+----------+
| COUNT(*) |
+----------+
|     4079 |
+----------+
1 row in set (0.00 sec)
*/

SELECT COUNT(*) FROM city WHERE CountryCode = 'BEN';
/**
+----------+
| COUNT(*) |
+----------+
|        4 |
+----------+
1 row in set (0.00 sec)
*/

START TRANSACTION;

DELETE FROM city WHERE CountryCode = 'BEN';

COMMIT;

SELECT COUNT(*) FROM city WHERE CountryCode = 'BEN';
/**
+----------+
| COUNT(*) |
+----------+
|        0 |
+----------+
1 row in set (0.00 sec)
*/

/**
Transaction does not work with MyISAM tables
*/
CREATE TABLE country_test
SELECT Code, Name FROM country;

ALTER TABLE country_test ENGINE=MyISAM;

SHOW CREATE TABLE country_test;

SELECT COUNT(*) FROM country_test;
/**
+----------+
| COUNT(*) |
+----------+
|      238 |
+----------+
1 row in set (0.00 sec)
*/

START TRANSACTION;

DELETE FROM country_test WHERE Code <> 'RUS';

ROLLBACK;

SELECT COUNT(*) FROM country_test;
/**
+----------+
| COUNT(*) |
+----------+
|        1 |
+----------+
1 row in set (0.00 sec)
*/

/**
Actually every transaction in InnoDB is a transaction. 
If operation can not be completed (e.g. power down or anything)
operation is rolled back automatically.

SET AUTOCOMMIT = { 0 | 1 }
Default AUTOCOMMIT value is 1.
If AUTOCOMMIT flag is OFF all operations are written to log, 
have to be either commited or [by default] rolled back.
*/

SHOW VARIABLES LIKE '%commit%';
/**
+-----------------------------------------+-------+
| Variable_name                           | Value |
+-----------------------------------------+-------+
| autocommit                              | ON    |
| binlog_group_commit_sync_delay          | 0     |
| binlog_group_commit_sync_no_delay_count | 0     |
| binlog_order_commits                    | ON    |
| innodb_api_bk_commit_interval           | 5     |
| innodb_commit_concurrency               | 0     |
| innodb_flush_log_at_trx_commit          | 2     |
| slave_preserve_commit_order             | OFF   |
+-----------------------------------------+-------+
8 rows in set (0.02 sec)
*/

SET autocommit = 0;

SHOW VARIABLES LIKE '%autocommit%';
/**
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| autocommit    | OFF   |
+---------------+-------+
1 row in set (0.00 sec)
*/

SELECT COUNT(*) FROM city;
/**
+----------+
| COUNT(*) |
+----------+
|     4075 |
+----------+
1 row in set (0.00 sec)
*/

DELETE FROM city WHERE CountryCode = 'ARG';

SELECT COUNT(*) FROM city;
/**
+----------+
| COUNT(*) |
+----------+
|     4018 |
+----------+
1 row in set (0.00 sec)
*/

ROLLBACK;

SELECT COUNT(*) FROM city;
/**
+----------+
| COUNT(*) |
+----------+
|     4075 |
+----------+
1 row in set (0.00 sec)
*/

DELETE FROM city WHERE CountryCode <> 'ARG';

SELECT COUNT(*) FROM city;
/**
+----------+
| COUNT(*) |
+----------+
|       63 |
+----------+
1 row in set (0.00 sec)
*/

-- TERMINATE SESSION
connect;

-- Transaction was not commited
SELECT COUNT(*) FROM city;
/**
+----------+
| COUNT(*) |
+----------+
|     4075 |
+----------+
1 row in set (0.00 sec)
*/


/**
TRANSACTION ISOLATION LEVEL
0 - Read Ucommitted (Dirty Read),
read uncommited changes performed by this transaction and all other (concurrent) transactions
1 - Read Committed
read all changes performed by this transaction and commited changes performed by concurrent transactions
2 - Repeatable Read (Snapshot)
read all changes performed by this transaction, all changes performed by other transaction after 
beginning of this one are not available
3 - [NOT IMPLEMENTED IN MYSQL] Serializabe 

MySQL may be configured to use level 0, 1, 2. Default level is 2.

Transaction level may be configured via `mysql.ini` 
[mysqld]
transaction-isolation = READ-COMMITTED

Transaction level can be set by via variable for current session or globally
*/

SHOW VARIABLES LIKE '%iso%';
/**
+-----------------------+-----------------+
| Variable_name         | Value           |
+-----------------------+-----------------+
| transaction_isolation | REPEATABLE-READ |
| tx_isolation          | REPEATABLE-READ |
+-----------------------+-----------------+
2 rows in set (0.00 sec)
*/

SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SHOW VARIABLES LIKE '%iso%';
/**
+-----------------------+------------------+
| Variable_name         | Value            |
+-----------------------+------------------+
| transaction_isolation | READ-UNCOMMITTED |
| tx_isolation          | READ-UNCOMMITTED |
+-----------------------+------------------+
2 rows in set (0.00 sec)
*/

connect;

SHOW VARIABLES LIKE '%iso%';
/**
+-----------------------+-----------------+
| Variable_name         | Value           |
+-----------------------+-----------------+
| transaction_isolation | REPEATABLE-READ |
| tx_isolation          | REPEATABLE-READ |
+-----------------------+-----------------+
2 rows in set (0.00 sec)
*/

-- EXAMPLE #1 READ UNCOMMITTED
/**
$ mysql -uroot -p world
*/

SELECT * FROM city WHERE id = 3580; 
/**
+------+--------+-------------+---------------+------------+
| ID   | Name   | CountryCode | District      | Population |
+------+--------+-------------+---------------+------------+
| 3580 | Moscow | RUS         | Moscow (City) |   19965000 |
+------+--------+-------------+---------------+------------+
1 row in set (0.04 sec)
*/
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

START TRANSACTION;

UPDATE city SET Name = 'New Moscow' WHERE id = 3580;

SELECT * FROM city WHERE id = 3580; 
/**
+------+------------+-------------+---------------+------------+
| ID   | Name       | CountryCode | District      | Population |
+------+------------+-------------+---------------+------------+
| 3580 | New Moscow | RUS         | Moscow (City) |   19965000 |
+------+------------+-------------+---------------+------------+
1 row in set (0.00 sec)
*/

-- hand over to other user

ROLLBACK;

/**
$ mysql -umike -p world
*/
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT * FROM city WHERE id = 3580; 
/**
+------+------------+-------------+---------------+------------+
| ID   | Name       | CountryCode | District      | Population |
+------+------------+-------------+---------------+------------+
| 3580 | New Moscow | RUS         | Moscow (City) |   19965000 |
+------+------------+-------------+---------------+------------+
1 row in set (0.00 sec)
*/

/**
EXPLANATION
Other users can see changes before they are commited, even if they are rolled back later on.
*/

-- EXAMPLE #2 READ COMMITED
/**
$ mysql -uroot -p world
*/
SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;

START TRANSACTION;

SELECT * FROM city WHERE id = 3580; 
/**
+------+--------+-------------+---------------+------------+
| ID   | Name   | CountryCode | District      | Population |
+------+--------+-------------+---------------+------------+
| 3580 | Moscow | RUS         | Moscow (City) |   19965000 |
+------+--------+-------------+---------------+------------+
1 row in set (0.00 sec)
*/

-- hand over to other user

SELECT * FROM city WHERE id = 3580; 
/**
+------+------------+-------------+---------------+------------+
| ID   | Name       | CountryCode | District      | Population |
+------+------------+-------------+---------------+------------+
| 3580 | New Moscow | RUS         | Moscow (City) |   19965000 |
+------+------------+-------------+---------------+------------+
1 row in set (0.00 sec)
*/

ROLLBACK;

/**
$ mysql -umike -p world
*/
SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;

SHOW VARIABLES LIKE '%iso%';

SELECT * FROM city WHERE id = 3580; 

UPDATE city SET Name = 'New Moscow' WHERE id = 3580;

SELECT * FROM city WHERE id = 3580; 

/**
EXPLANATION
Within transaction we can see changes commited by other users.
*/

-- EXAMPLE #3 REPEATABLE READ
/**
$ mysql -uroot -p world
*/
SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;

SHOW VARIABLES LIKE '%iso%';
/**
+-----------------------+-----------------+
| Variable_name         | Value           |
+-----------------------+-----------------+
| transaction_isolation | REPEATABLE-READ |
| tx_isolation          | REPEATABLE-READ |
+-----------------------+-----------------+
2 rows in set (0.00 sec)
*/

START TRANSACTION;

SELECT * FROM city WHERE id = 3580; 
/**
+------+--------+-------------+---------------+------------+
| ID   | Name   | CountryCode | District      | Population |
+------+--------+-------------+---------------+------------+
| 3580 | Moscow | RUS         | Moscow (City) |   19965000 |
+------+--------+-------------+---------------+------------+
1 row in set (0.00 sec)
*/

-- hand over to other user

SELECT * FROM city WHERE id = 3580; 
/**
+------+--------+-------------+---------------+------------+
| ID   | Name   | CountryCode | District      | Population |
+------+--------+-------------+---------------+------------+
| 3580 | Moscow | RUS         | Moscow (City) |   19965000 |
+------+--------+-------------+---------------+------------+
1 row in set (0.00 sec)
*/

COMMIT;

/**
+------+------------+-------------+---------------+------------+
| ID   | Name       | CountryCode | District      | Population |
+------+------------+-------------+---------------+------------+
| 3580 | New Moscow | RUS         | Moscow (City) |   19965000 |
+------+------------+-------------+---------------+------------+
1 row in set (0.00 sec)
*/


/**
$ mysql -umike -p world
*/
SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;

SHOW VARIABLES LIKE '%iso%';

SELECT * FROM city WHERE id = 3580; 

UPDATE city SET Name = 'New Moscow' WHERE id = 3580;

SELECT * FROM city WHERE id = 3580; 
/**
+------+------------+-------------+---------------+------------+
| ID   | Name       | CountryCode | District      | Population |
+------+------------+-------------+---------------+------------+
| 3580 | New Moscow | RUS         | Moscow (City) |   19965000 |
+------+------------+-------------+---------------+------------+
1 row in set (0.00 sec)
*/

-- hand over to @root

/**
EXPLANATION
Within transaction we can NOT see changes commited by other users before transaction is completed.
Within transaction we work with a database snapshot made before transaction begins.
*/