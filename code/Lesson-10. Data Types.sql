-- Data Types
-- CHAR vs VARCHAR
CREATE DATABASE tmp;
USE tmp;
CREATE TABLE dogs (
    name CHAR(5),
    breed VARCHAR(5)
);

SET @@global.sql_mode= '';

INSERT INTO dogs (name, breed)
VALUES
    ('Bob', 'Fox'),
    ('Dobby', 'Corgi'),
    ('Jorge', 'Keeshond');

SHOW WARNINGS;
/*
+---------+------+--------------------------------------------+
| Level   | Code | Message                                    |
+---------+------+--------------------------------------------+
| Warning | 1265 | Data truncated for column 'breed' at row 3 |
+---------+------+--------------------------------------------+
1 row in set (0.00 sec)
*/

SELECT name, LENGTH(name), breed, LENGTH(breed) FROM dogs;
/*
+-------+--------------+-------+---------------+
| name  | LENGTH(name) | breed | LENGTH(breed) |
+-------+--------------+-------+---------------+
| Bob   |            3 | Fox   |             3 |
| Dobby |            5 | Corgi |             5 |
| Jorge |            5 | Keesh |             5 |
+-------+--------------+-------+---------------+
3 rows in set (0.02 sec)
*/

-- NUMERIC DATA

-- INT

-- DECIMAL(digitsTotal, digitsDecimal), 
-- digitsTotal - maximum number of digits, [1..65] 
-- digitsDecimal - number of digits after a decimal point, [0..30], lte digitalsTotal

CREATE TABLE numbers (
    item DECIMAL(5,2)
);

INSERT INTO numbers (item)
VALUES 
    (7), 
    (654321), 
    (12.34), 
    (12.34567);
/*
Query OK, 4 rows affected, 2 warnings (0.04 sec)
Records: 4  Duplicates: 0  Warnings: 2
*/
SHOW WARNINGS;
/*
+---------+------+-----------------------------------------------+
| Level   | Code | Message                                       |
+---------+------+-----------------------------------------------+
| Warning | 1264 | Out of range value for column 'item' at row 2 |
| Note    | 1265 | Data truncated for column 'item' at row 4     |
+---------+------+-----------------------------------------------+
2 rows in set (0.00 sec)
*/
SELECT * FROM numbers;
/*
+--------+
| item   |
+--------+
|   7.00 |
| 999.99 |
|  12.34 |
|  12.35 |
+--------+
4 rows in set (0.02 sec)
*/
-- Notes
-- if number too big the largest possible number is stored
-- if number of digit after decimal point is too large number is rounded

-- FLOAT && DOUBLE
/**
The difference between DECIMAL and FLOAT/DOUBLE
The DECIMAL data type is a fixed point type and calculatins are exact. 
The DECIMAL data type has several synonims: NUMERIC, DEC, FIXED.
The FLOAT and DOUBLE data types are floating point types and calculations are approximaate.
FLOAT/DOUBLE data type has synonims DOUBLE PRECISION and REAL

FLOAT and DOUBLE store larger numbers using less space,
BUT it comes with a cost of precision

The difference between FLOAT and DOUBLE 
is mostly technical -> the way numbers are stored into database (base 10 vs base 2)

Data type   | Memory Needed | Precision Issues 
FLOAT       | 4 bytes       | ~7 digits
DOUBLE      | 8 bytes       | ~15 digits
*/

DROP DATABASE numbers;

CREATE TABLE numbers (
    item FLOAT
);

INSERT INTO numbers VALUES (123.456);

INSERT INTO numbers VALUES (1234567.89);

INSERT INTO numbers VALUES (123456789);

SELECT * FROM numbers;
/*
+-----------+
| item      |
+-----------+
|   123.456 |
|   1234570 |
| 123457000 |
+-----------+
3 rows in set (0.00 sec)
*/

-- DATES and TIME

-- DATE, e.g. birthday
-- Format 'YYYY-MM-DD'

-- TIME, e.g. alarm or scheduler
-- Format 'HH:MM:SS'

-- DATETIME
-- Format 'YYYY-MM-DD HH:MM:SS'

CREATE TABLE people (
    name VARCHAR(100),
    birthday DATE,
    birthtime TIME,
    dt DATETIME
);

INSERT INTO people (name, birthday, birthtime, dt)
VALUES 
    ('John', '1998-09-16', '01:01:01', '1998-09-16 01:01:01'),
    ('Jane', '2002-10-24', '02:02:02', '2002-10-24 02:02:02');

-- NOW()
INSERT INTO people (name, birthday, birthtime, dt)
VALUES ('Jim', CURDATE(), CURTIME(), NOW());

-- FORMATTING DATES
SELECT name, birthday FROM people;
SELECT name, birthday, DAY(birthday) as day FROM people;
SELECT name, birthday, DAYOFWEEK(birthday) as day FROM people;
SELECT name, birthday, DAYNAME(birthday) as day FROM people;
SELECT name, birthday, DAYOFYEAR(birthday) as day FROM people;
SELECT name, birthday, MONTH(birthday) as month, MONTHNAME(birthday) as monthname FROM people;
-- takes into account leap year 

SELECT name, dt, DAYOFYEAR(dt) as day FROM people;
SELECT name, dt, HOUR(dt) as hours, MINUTE(dt) as minites FROM people;

-- October 25th 2002
SELECT CONCAT( MONTHNAME(dt), ' ', DAY(dt), ' ', YEAR(dt)) FROM people;
-- There is a better way
-- DATE_FORMAT()
-- https://dev.mysql.com/doc/refman/5.5/en/date-and-time-functions.html#function_date-format
SELECT name, DATE_FORMAT(dt, '%M %D, %Y') FROM people;
SELECT CONCAT(name, ' was born on ', DATE_FORMAT(dt, '%W'), ' at ', DATE_FORMAT(dt, '%r')) FROM people;

-- DATETIME MATH
-- DATEDIFF()
SELECT name, birthday, DATEDIFF(NOW(), birthday) FROM people;

-- DATE_ADD()/ADDDATE()
-- DATE_SUB()/SUBDATE()
-- +/-
-- https://dev.mysql.com/doc/refman/5.5/en/date-and-time-functions.html#function_date-add
-- https://dev.mysql.com/doc/refman/5.5/en/expressions.html#temporal-intervals
SELECT name, birthday, DATE_ADD(birthday, INTERVAL 1 MONTH) FROM people;
SELECT name, dt, DATE_ADD(dt, INTERVAL '1 1' DAY_HOUR) FROM people;
SELECT name, birthday, birthday + INTERVAL 1 MONTH FROM people;
SELECT name, dt, dt + INTERVAL 1 DAY + INTERVAL 1 HOUR FROM people;

-- TIMESTAMP
CREATE TABLE comments (
    body VARCHAR(255),
    created_at TIMESTAMP DEFAULT NOW()
);

INSERT INTO comments (body) VALUES ('Lorem ipsum dolorem sic...');
INSERT INTO comments (body) VALUES ('LOL! Very funny article');
INSERT INTO comments (body) VALUES ('This is just stupid');

SELECT * FROM comments ORDER BY created_at DESC;

CREATE TABLE articles (
    body VARCHAR(255),
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW() ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO articles (body) VALUES ('Lorem ipsum dolorem sic...');
INSERT INTO articles (body) VALUES ('New article on some cool stuff');
INSERT INTO articles (body) VALUES ('Year review');

SELECT * FROM articles ORDER BY updated_at DESC;
/*
+--------------------------------+---------------------+---------------------+
| body                           | created_at          | updated_at          |
+--------------------------------+---------------------+---------------------+
| Year review                    | 2019-12-07 22:50:32 | 2019-12-07 22:50:32 |
| New article on some cool stuff | 2019-12-07 22:50:25 | 2019-12-07 22:50:25 |
| Lorem ipsum dolorem sic...     | 2019-12-07 22:50:19 | 2019-12-07 22:50:19 |
+--------------------------------+---------------------+---------------------+
*/
UPDATE articles 
SET body = 'First article in the database'
WHERE body = 'Lorem ipsum dolorem sic...';

SELECT * FROM articles ORDER BY updated_at DESC;
/*
+--------------------------------+---------------------+---------------------+
| body                           | created_at          | updated_at          |
+--------------------------------+---------------------+---------------------+
| First article in the database  | 2019-12-07 22:50:19 | 2019-12-07 22:53:36 |
| Year review                    | 2019-12-07 22:50:32 | 2019-12-07 22:50:32 |
| New article on some cool stuff | 2019-12-07 22:50:25 | 2019-12-07 22:50:25 |
+--------------------------------+---------------------+---------------------+
*/

-- Exercises
CREATE TABLE inventory (
    item_name VARCHAR(100),
    price DECIMAL(8,2),
    quantity INT
);

INSERT INTO inventory (item_name, price, quantity)
VALUES ('book', 123456.78, 100);

SELECT * FROM inventory;

SELECT CURTIME();
SELECT CURDATE();

-- What week day is today
SELECT DAYOFWEEK(CURDATE()); -- 1
SELECT DATE_FORMAT(NOW(), '%w'); -- 0

SELECT DAYNAME(CURDATE());
SELECT DAYNAME(NOW());
SELECT DATE_FORMAT(NOW(), '%W');

-- mm/dd/yyyy
SELECT DATE_FORMAT(NOW(), '%m/%d/%Y');

-- January 2nd at 3:15
-- April 1st at 10:18
CREATE TABLE dates (
    dt DATETIME
);

INSERT INTO dates (dt) 
VALUES
    ('2019-01-02 03:15:00'),
    ('2019-04-01 10:18:00');

SELECT dt, DATE_FORMAT(dt, '%M %D at %k:%i') from dates;

CREATE TABLE tweets (
    username VARCHAR(100),
    body VARCHAR(255),
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW() ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO tweets (username, body) VALUES ('john', 'some tweet content');
INSERT INTO tweets (username, body) VALUES ('jane', 'update status');

SELECT * FROM tweets;
SELECT * FROM tweets ORDER BY updated_at DESC;

UPDATE tweets SET body = NULL WHERE username = 'john';
SELECT * FROM tweets ORDER BY updated_at DESC;
