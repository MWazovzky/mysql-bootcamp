/**
Preparation
1. configure mysql to use UTF-8
[client]
default-character-set = utf8
...
[mysqld]
init_connect=‘SET collation_connection = utf8_unicode_ci’
character-set-server = utf8
collation-server = utf8_unicode_ci

2. Make surer loaded data is UTF-8 encoded
*/
source books_dev.sql;

CREATE FULLTEXT INDEX ixFullText ON books_dev (title, author_last_name);

-- IN NATURAL LANGUAGE MODE
SELECT * 
FROM books_dev 
WHERE MATCH (title, author_last_name) AGAINST ('mysql Зандстра');

SELECT title, author_last_name 
FROM books_dev 
WHERE MATCH (title, author_last_name) AGAINST ('mysql Зандстра' IN NATURAL LANGUAGE MODE);

SELECT title, author_last_name 
FROM books_dev 
WHERE MATCH (title, author_last_name) AGAINST ('основы программирования' IN NATURAL LANGUAGE MODE);

-- IN BOOLEAN MODE
SELECT title, author_last_name 
FROM books_dev 
WHERE MATCH (title, author_last_name) AGAINST ('+mysql' IN BOOLEAN MODE);
/**
+---------------------------------------------------------------------------------------------------+--------------------+
| title                                                                                             | author_last_name   |
+---------------------------------------------------------------------------------------------------+--------------------+
| MySQL                                                                                             | Дюбуа              |
| Разработка Web-приложений с помощью PHP и MySQL. 4-е издание                                      | Веллинг            |
| Изучаем PHP и MySQL                                                                               | Моррисон           |
| Обеспечение высокой доступности систем на основе MySQL                                            | Талманн            |
| PHP и MySQL: создание интернет-магазина, 2-е издание                                              | Дари               |
| MySQL 5                                                                                           | Кузнецов           |
| MySQL. Оптимизация производительности, 2-е издание                                                | Зайцев             |
| PHP и MySQL. Исчерпывающее руководство. 2-е изд.                                                  | Маклафлин          |
+---------------------------------------------------------------------------------------------------+--------------------+
8 rows in set (0.02 sec)
*/

SELECT title, author_last_name 
FROM books_dev 
WHERE MATCH (title, author_last_name) AGAINST ('+mysql -Дюбуа' IN BOOLEAN MODE);
/**
+---------------------------------------------------------------------------------------------------+--------------------+
| title                                                                                             | author_last_name   |
+---------------------------------------------------------------------------------------------------+--------------------+
| Разработка Web-приложений с помощью PHP и MySQL. 4-е издание                                      | Веллинг            |
| Изучаем PHP и MySQL                                                                               | Моррисон           |
| Обеспечение высокой доступности систем на основе MySQL                                            | Талманн            |
| PHP и MySQL: создание интернет-магазина, 2-е издание                                              | Дари               |
| MySQL 5                                                                                           | Кузнецов           |
| MySQL. Оптимизация производительности, 2-е издание                                                | Зайцев             |
| PHP и MySQL. Исчерпывающее руководство. 2-е изд.                                                  | Маклафлин          |
+---------------------------------------------------------------------------------------------------+--------------------+
7 rows in set (0.02 sec)
*/

-- Use placeholder
SELECT title, author_last_name 
FROM books_dev 
WHERE MATCH (title, author_last_name) AGAINST ('+Java*' IN BOOLEAN MODE);
/**
+-----------------------------------------------------------------------------------------------------------------+----------------------+
| title                                                                                                           | author_last_name     |
+-----------------------------------------------------------------------------------------------------------------+----------------------+
| JavaScript. Подробное руководство                                                                               | Макфарланд           |
| Графика на JavaScript                                                                                           | Чекко                |
| CoffeeScript. Второе дыхание JavaScript                                                                         | Бейтс                |
| Разработка приложений для Windows 8 на HTML5 и JavaScript                                                       | Esposito             |
| JavaScript и jQuery. Исчерпывающее руководство                                                                  | Макфарланд           |
| JavaScript. Карманный справочник                                                                                | Флэнаган             |
| JavaScript. Подробное руководство, 5-е издание                                                                  | Флэнаган             |
| jQuery. Подробное руководство по продвинутому JavaScript, 2-е издание                                           | Кац                  |
+-----------------------------------------------------------------------------------------------------------------+----------------------+
8 rows in set (0.02 sec)
*/

SELECT title, author_last_name 
FROM books_dev 
WHERE MATCH (title, author_last_name) AGAINST ('+Java* -Макфарланд -jQuery' IN BOOLEAN MODE);
/**
+-------------------------------------------------------------------------------------+------------------+
| title                                                                               | author_last_name |
+-------------------------------------------------------------------------------------+------------------+
| Графика на JavaScript                                                               | Чекко            |
| CoffeeScript. Второе дыхание JavaScript                                             | Бейтс            |
| Разработка приложений для Windows 8 на HTML5 и JavaScript                           | Esposito         |
| JavaScript. Карманный справочник                                                    | Флэнаган         |
| JavaScript. Подробное руководство, 5-е издание                                      | Флэнаган         |
+-------------------------------------------------------------------------------------+------------------+
5 rows in set (0.02 sec)
*/

SELECT 
    SUBSTRING(title, 1, 30), 
    CONCAT(author_last_name, ',', author_first_name) AS name,
    MATCH (title, author_last_name) AGAINST ('android' IN NATURAL LANGUAGE MODE) AS score
FROM books_dev 
WHERE MATCH (title, author_last_name) AGAINST ('android' IN BOOLEAN MODE);


SELECT @@ft_min_word_len;
/**
+-------------------+
| @@ft_min_word_len |
+-------------------+
|                 4 |
+-------------------+
1 row in set (0.00 sec)
*/

/**
CONFIGURE FULLTEXT SEARCH MIN WORD LENGTH FOR MyISAM tables
mysql.ini: set ft_min_word_len = 3
restart server
rebuid indexes: REPAIR TABLE books_dev QUICK;
*/
SELECT 
    SUBSTRING(title, 1, 30), 
    CONCAT(author_last_name, ',', author_first_name) AS name,
    MATCH (title, author_last_name) AGAINST ('php' IN NATURAL LANGUAGE MODE) AS score
FROM books_dev 
WHERE MATCH (title, author_last_name) AGAINST ('php' IN BOOLEAN MODE);

DROP INDEX ixFullText ON books_dev;