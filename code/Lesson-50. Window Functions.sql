/**
Window Function
Allows run fuction over rows susbset without grouping them.
https://dev.mysql.com/doc/refman/8.0/en/window-functions.html
*/

USE world;

CREATE TABLE sales (
    id SERIAL PRIMARY KEY,
    country VARCHAR(100),
    product VARCHAR(100),
    amount INT
);

INSERT INTO sales (country, product, amount) VALUES
('Russia', 'computers', 1000),
('Russia', 'mobiles', 2500),
('Germany', 'computers', 700),
('Germany', 'auto', 250),
('Russia', 'software', 7400),
('England', 'toys', 3400),
('Russia', 'computers', 600);

SELECT country, product, SUM(amount)
FROM sales
GROUP BY country, product;

SELECT 
    ROW_NUMBER() OVER(PARTITION BY country) AS num,
    country, product, amount,
    SUM(amount) OVER (PARTITION BY country) AS county_sales,
    SUM(amount) OVER () AS total_s
FROM sales;
ORDER BY country;

SELECT 
    ROW_NUMBER() OVER w AS 'row_number',
    country, product, amount,
    SUM(amount) OVER w AS country_sales,
    SUM(amount) OVER () AS total_sales
FROM sales
WINDOW w AS (PARTITION BY country);
