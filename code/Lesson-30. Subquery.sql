-- EXAMPLES
USE world;

SELECT Language, CountryCode, Percentage
FROM countrylanguage
WHERE CountryCode = (
    SELECT Code 
    FROM country 
    WHERE Name = 'Russian Federation'
);
-- ERROR is subquery returns more then one row

SELECT Language, CountryCode, Percentage
FROM countrylanguage
WHERE CountryCode IN (
    SELECT CountryCode 
    FROM countrylanguage
    WHERE Language = 'Russian' AND IsOfficial = 'T'
)
ORDER BY CountryCode, Percentage DESC;

SELECT * 
FROM city
WHERE CountryCode = (
    SELECT Code
    FROM country
    ORDER BY Population DESC
    LIMIT 1
)
ORDER BY Population DESC;

-- IN()
SELECT * 
FROM city
WHERE Population > 1E6 AND CountryCode IN (
    SELECT Code FROM country WHERE Name LIKE '%rus%'
);

-- ANY()
SELECT * 
FROM city
WHERE Population > 1E6 AND CountryCode = ANY (
    SELECT Code FROM country WHERE Name LIKE '%rus%'
);

-- Greater the the smallest in the set
SELECT city.Name, country.Name, city.Population
FROM city
INNER JOIN country ON city.CountryCode = country.Code
WHERE country.Continent = 'Africa' AND city.Population > ANY (
    SELECT Population FROM country WHERE Continent = 'Europe'
)
ORDER BY Population DESC;

-- ALL()
-- Greater the the largest in the set
SELECT Name, CountryCode, Population
FROM city
WHERE Population > ALL (
    SELECT Population FROM city WHERE CountryCode = 'CHN'
)
ORDER BY Population DESC;

-- SOME()
-- equivalent to ANY()
SELECT * 
FROM city
WHERE Population > 1E6 AND CountryCode = SOME (
    SELECT Code FROM country WHERE Name LIKE '%rus%'
);

-- BETWEEN
SELECT COUNT(*)
FROM city
WHERE Population BETWEEN 1E6 AND 1E7;

-- EXISTS/NOT EXISTS
SELECT Name 
FROM country
WHERE EXISTS (
    SELECT * 
    FROM city
    WHERE city.CountryCode = country.Code AND Population > 5e6
);

SELECT Name 
FROM country
WHERE NOT EXISTS (
    SELECT * 
    FROM city
    WHERE city.CountryCode = country.Code AND Population > 1e3
);

-- There are often more efficient ways to solve problem then exists