-- One to Many
CREATE TABLE customers (
    customer_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(100),
    last_name VARCHAR(100),
    email VARCHAR(100)
);

INSERT INTO customers (first_name, last_name, email)
VALUES 
    ('John', 'Dow', 'john@example.come'),
    ('Jane', 'Dow', 'jane@example.come'),
    ('Bill', 'Foo', 'bill@example.come');

SELECT * FROM customers;

CREATE TABLE orders (
    order_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    customer_id INT NOT NULL,
    price DECIMAL(7,2),
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW() ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id) ON DELETE CASCADE
);

INSERT INTO orders (customer_id, price)
VALUES (2, 99.98);

INSERT INTO orders (customer_id, price)
VALUES (2, 12.49);

INSERT INTO orders (customer_id, price)
VALUES (2, 200.00);

INSERT INTO orders (customer_id, price)
VALUES (1, 399.99);

SELECT * FROM orders;

-- IMPLICIT INNER JOIN
SELECT 
    orders.order_id, 
    orders.price, 
    orders.created_at, 
    CONCAT(customers.first_name, ' ', customers.last_name) AS customer 
FROM orders, customers
WHERE orders.customer_id = customers.customer_id;

-- EXPLICIT INNER JOIN (always use explicit joins!)
SELECT 
    orders.order_id, 
    orders.price, 
    orders.created_at, 
    CONCAT(customers.first_name, ' ', customers.last_name) AS customer 
FROM orders
INNER JOIN customers ON orders.customer_id = customers.customer_id;

SELECT 
    CONCAT(customers.first_name, ' ', customers.last_name) AS customer, 
    orders.order_id, 
    orders.price, 
    orders.created_at
FROM customers
INNER JOIN orders ON orders.customer_id = customers.customer_id;

-- Things can be joined on any condition
SELECT 
    orders.order_id, 
    orders.price, 
    orders.created_at, 
    customers.customer_id, 
    CONCAT(customers.first_name, ' ', customers.last_name) AS customer 
FROM orders
INNER JOIN customers ON orders.order_id = customers.customer_id;

-- LEFT JOIN
SELECT 
    CONCAT(customers.first_name, ' ', customers.last_name) AS customer, 
    orders.order_id, 
    orders.price, 
    orders.created_at
FROM customers
LEFT JOIN orders ON orders.customer_id = customers.customer_id;

SELECT 
    CONCAT(customers.first_name, ' ', customers.last_name) AS customer, 
    orders.order_id, 
    orders.price, 
    orders.created_at
FROM customers
RIGHT JOIN orders ON orders.customer_id = customers.customer_id;

SELECT 
    orders.order_id, 
    orders.price, 
    orders.created_at, 
    CONCAT(customers.first_name, ' ', customers.last_name) AS customer 
FROM orders
RIGHT JOIN customers ON orders.customer_id = customers.customer_id;

-- JOINs can be handled like regular tables: WHERE, ORDER BY, GROUP BY ...
SELECT 
    CONCAT(customers.first_name, ' ', customers.last_name) AS customer, 
    COUNT(*), 
    SUM(orders.price) AS orders_total
FROM customers
JOIN orders ON orders.customer_id = customers.customer_id
GROUP BY customers.customer_id
ORDER BY orders_total DESC;

-- Why would we need a left join?
SELECT 
    customers.first_name AS first, 
    customers.last_name AS last, 
    IFNULL(SUM(orders.price), 0) AS orders_total
FROM customers
LEFT JOIN orders ON orders.customer_id = customers.customer_id
GROUP BY customers.customer_id
ORDER BY orders_total DESC;

-- RIGHT JOIN EXAMPLE
DROP TABLE orders, customers;

CREATE TABLE customers (
    customer_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(100),
    last_name VARCHAR(100),
    email VARCHAR(100)
);

INSERT INTO customers (first_name, last_name, email)
VALUES 
    ('John', 'Dow', 'john@example.come'),
    ('Jane', 'Dow', 'jane@example.come'),
    ('Bill', 'Foo', 'bill@example.come');

SELECT * FROM customers;

CREATE TABLE orders (
    order_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    customer_id INT NOT NULL,
    price DECIMAL(7,2),
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW() ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO orders (customer_id, price)
VALUES 
    (2, 99.98),
    (2, 12.49),
    (2, 200.00),
    (1, 399.99),
    (101, 199.99),
    (102, 77.49);

SELECT * FROM orders;

SELECT 
    IFNULL(customers.last_name, 'Missing') AS last,
    IFNULL(customers.first_name, '') AS first,
    SUM(orders.price) AS orders_total
FROM customers
RIGHT JOIN orders ON customers.customer_id = orders.customer_id
GROUP BY last, first
ORDER BY orders_total DESC;

-- ON DELETE CASCADE

-- EXCERCISES
CREATE TABLE students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100)
);

CREATE TABLE papers (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255),
    grade INT,
    student_id INT,
    FOREIGN KEY (student_id) REFERENCES students(id) ON DELETE CASCADE
);

INSERT INTO students (name)
VALUES ('John'), ('Jane'), ('Bill'), ('Vasya'), ('Mary');

SELECT * FROM students;

INSERT INTO papers (student_id, title, grade ) 
VALUES
    (1, 'My First Book Report', 60),
    (1, 'My Second Book Report', 75),
    (2, 'Russian Lit Through The Ages', 94),
    (2, 'De Montaigne and The Art of The Essay', 98),
    (4, 'Borges and Magical Realism', 89);

SELECT * FROM papers;

SELECT students.name, papers.title, papers.grade 
FROM papers
LEFT JOIN students ON papers.student_id = students.id
ORDER BY grade DESC;
/*
+-------+---------------------------------------+-------+
| name  | title                                 | grade |
+-------+---------------------------------------+-------+
| Jane  | De Montaigne and The Art of The Essay |    98 |
| Jane  | Russian Lit Through The Ages          |    94 |
| Vasya | Borges and Magical Realism            |    89 |
| John  | My Second Book Report                 |    75 |
| John  | My First Book Report                  |    60 |
+-------+---------------------------------------+-------+
5 rows in set (0.02 sec)
*/

SELECT students.id, students.name, papers.title, papers.grade 
FROM students
LEFT JOIN papers ON students.id = papers.student_id
ORDER BY students.id;
/*
+----+-------+---------------------------------------+-------+
| id | name  | title                                 | grade |
+----+-------+---------------------------------------+-------+
|  1 | John  | My First Book Report                  |    60 |
|  1 | John  | My Second Book Report                 |    75 |
|  2 | Jane  | De Montaigne and The Art of The Essay |    98 |
|  2 | Jane  | Russian Lit Through The Ages          |    94 |
|  3 | Bill  | NULL                                  |  NULL |
|  4 | Vasya | Borges and Magical Realism            |    89 |
|  5 | Mary  | NULL                                  |  NULL |
+----+-------+---------------------------------------+-------+
7 rows in set (0.00 sec)
*/

SELECT 
    students.name, 
    IFNULL(papers.title, 'MISSING'), 
    IFNULL(papers.grade, 0) 
FROM students
LEFT JOIN papers ON students.id = papers.student_id
ORDER BY students.id;
/*
+-------+---------------------------------------+-------------------------+
| name  | IFNULL(papers.title, 'MISSING')       | IFNULL(papers.grade, 0) |
+-------+---------------------------------------+-------------------------+
| John  | My First Book Report                  |                      60 |
| John  | My Second Book Report                 |                      75 |
| Jane  | De Montaigne and The Art of The Essay |                      98 |
| Jane  | Russian Lit Through The Ages          |                      94 |
| Bill  | MISSING                               |                       0 |
| Vasya | Borges and Magical Realism            |                      89 |
| Mary  | MISSING                               |                       0 |
+-------+---------------------------------------+-------------------------+
7 rows in set (0.02 sec)
*/

SELECT 
    students.name, 
    COUNT(*) AS 'count',
    IFNULL(AVG(papers.grade), '0') AS average,
    CASE
        WHEN AVG(papers.grade) IS NULL THEN 'FAILING'
        WHEN AVG(papers.grade) > 70 THEN 'PASSING'
        ELSE 'FAILING'
    END AS passing_status
FROM students
LEFT JOIN papers ON students.id = papers.student_id
GROUP BY students.id
ORDER BY average DESC;
/*
+-------+-------+---------+----------------+
| name  | count | average | passing_status |
+-------+-------+---------+----------------+
| Jane  |     2 | 96.0000 | PASSING        |
| Vasya |     1 | 89.0000 | PASSING        |
| John  |     2 | 67.5000 | FAILING        |
| Mary  |     1 | 0       | FAILING        |
| Bill  |     1 | 0       | FAILING        |
+-------+-------+---------+----------------+
5 rows in set (0.02 sec)
*/

