-- UNION
USE app_node_db;

SELECT CONCAT(first_name, ' ', last_name), created_at
FROM users
WHERE first_name = 'John' AND last_name LIKE 'A%'
UNION DISTINCT
SELECT user, NOW()
FROM mysql.user;

-- Default mode is DISTINCT
-- There can be multiple UNION operators in one query
(SELECT CONCAT(first_name, ' ', last_name), created_at
FROM users
WHERE first_name = 'John' AND last_name LIKE 'A%')
UNION ALL
(SELECT CONCAT(first_name, ' ', last_name), created_at
FROM users
WHERE first_name = 'John' AND last_name LIKE 'Z%')
UNION ALL
(SELECT user, NOW()
FROM mysql.user);


-- ORDER BY ORDERS ALL ROWS
CREATE TABLE num1 (
    num INT
);

INSERT INTO num1 (num) VALUES 
(1), (11), (111);

CREATE TABLE num2 (
    num INT
);

INSERT INTO num2 (num) 
VALUES (2), (22), (222);

SELECT num AS num_value
FROM num1 
UNION 
SELECT num AS num_value
FROM num2 
ORDER BY num_value;
/**
+------+
| num  |
+------+
|    1 |
|    2 |
|   11 |
|   22 |
|  111 |
|  222 |
+------+
*/


