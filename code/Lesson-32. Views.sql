-- VIEWS
-- https://dev.mysql.com/doc/refman/8.0/en/create-view.html
USE world;

CREATE VIEW  view_city_rus
AS SELECT * 
FROM city 
WHERE CountryCode = 'RUS';

-- View can be used as a table
DESC view_city_rus;
/**
+-------------+----------+------+-----+---------+-------+
| Field       | Type     | Null | Key | Default | Extra |
+-------------+----------+------+-----+---------+-------+
| ID          | int(11)  | NO   |     | 0       |       |
| Name        | char(35) | NO   |     |         |       |
| CountryCode | char(3)  | NO   |     |         |       |
| District    | char(20) | NO   |     |         |       |
| Population  | int(11)  | NO   |     | 0       |       |
+-------------+----------+------+-----+---------+-------+
5 rows in set (0.02 sec)
*/


SELECT * FROM view_city_rus;
/**
View is just an interface to data 
View data are still stored in the table(s)
*/
SHOW TABLES;
/**
+-----------------+
| Tables_in_world |
+-----------------+
| city            |
| country         |
| countrylanguage |
| view_city_rus   |
+-----------------+
4 rows in set (0.00 sec)
*/

connect;

SHOW TABLES;
/**
+-----------------+
| Tables_in_world |
+-----------------+
| city            |
| country         |
| countrylanguage |
| view_city_rus   |
+-----------------+
4 rows in set (0.00 sec)
*/

/**
Views can be updated
There are many limitations, you'd better not try it
https://dev.mysql.com/doc/refman/5.7/en/view-updatability.html
*/
UPDATE view_city_rus
SET Population = 15E6
WHERE Name = 'Moscow';

SELECT * FROM view_city_rus;

DROP VIEW view_city_rus;

CREATE VIEW view_city_forecast
AS SELECT Name, Population, Population * 1.1, Population * 1.2
FROM city
WHERE Population > 1e6 AND Name LIKE 'm%'
ORDER BY Population DESC
LIMIT 10;

SELECT * FROM view_city_forecast;

DROP VIEW view_city_forecast;


/**
Views can be created with OR REPLACE
*/
CREATE OR REPLACE VIEW country_view AS
SELECT Code, Name, Population
FROM country;

/**
Views can be created with IF EXISTS
*/
DROP VIEW country_view;
/**
Query OK, 0 rows affected (0.04 sec)
*/

DROP VIEW country_view;
/**
ERROR 1051 (42S02): Unknown table 'world.country_view'
*/

DROP VIEW IF EXISTS country_view;
/**
Query OK, 0 rows affected, 1 warning (0.04 sec)
*/

SHOW WARNINGS;
/**
+-------+------+------------------------------------+
| Level | Code | Message                            |
+-------+------+------------------------------------+
| Note  | 1051 | Unknown table 'world.country_view' |
+-------+------+------------------------------------+
1 row in set (0.00 sec)
*/