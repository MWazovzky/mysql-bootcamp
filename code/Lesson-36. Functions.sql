/**
Function
Functions have return value, must have type
Funsctions are stored in database.
Show functions listing:
SHOW FUNCTION STATUS [LIKE 'pattern' | WHERE search_condition]
*/

DROP FUNCTION IF EXISTS sp_large_countries_count;
DELIMITER $$

CREATE FUNCTION sp_large_countries_count(trashhold INTEGER)
RETURNS INTEGER 
BEGIN
    DECLARE populationTrashhold INTEGER(11);
    DECLARE result INTEGER(11);

    IF trashhold IS NULL
        THEN 
            SELECT AVG(Population) INTO populationTrashhold 
            FROM country;
        ELSE SET populationTrashhold = trashhold;
    END IF;

    SELECT COUNT(*) INTO result
    FROM country
    WHERE Population > populationTrashhold;

    RETURN (result);
END $$

DELIMITER ;

SELECT sp_large_countries_count(1e8);
SELECT sp_large_countries_count(1e6);
SELECT sp_large_countries_count(NULL);


SHOW FUNCTION STATUS LIKE '%countr%';
SHOW FUNCTION STATUS WHERE Db = 'world';
/**
+-------+--------------------------+----------+---------+---------------------+---------------------+---------------+---------+----------------------+----------------------+--------------------+
| Db    | Name                     | Type     | Definer | Modified            | Created             | Security_type | Comment | character_set_client | collation_connection | Database Collation |
+-------+--------------------------+----------+---------+---------------------+---------------------+---------------+---------+----------------------+----------------------+--------------------+
| world | sp_large_countries_count | FUNCTION | root@%  | 2020-01-08 11:26:14 | 2020-01-08 11:26:14 | DEFINER       |         | utf8                 | utf8_general_ci      | utf8_unicode_ci    |
+-------+--------------------------+----------+---------+---------------------+---------------------+---------------+---------+----------------------+----------------------+--------------------+
1 row in set (0.00 sec)
*/

DROP FUNCTION IF EXISTS sp_large_countries_count;

-- EXAMPLE #2
DROP FUNCTION IF EXISTS surface_per_person;

DELIMITER $$

CREATE FUNCTION surface_per_person(c CHAR(3))
RETURNS FLOAT
BEGIN
    DECLARE result FLOAT;
    SELECT SurfaceArea / Population INTO result
    FROM country
    WHERE Code = c;
    RETURN result;
END $$

DELIMITER ;

SELECT surface_per_person('RUS');
SELECT surface_per_person('ROM');
