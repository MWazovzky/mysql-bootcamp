-- Variables
-- Available only for current session, removed is session terminated
SET @local_var = 123;

SELECT @local_var;
/**
+------------+
| @local_var |
+------------+
|        123 |
+------------+
1 row in set (0.00 sec)
*/

-- Reconnect to the server. Current session is destroyed
connect
SELECT @local_var;
/**
+------------+
| @local_var |
+------------+
| NULL       |
+------------+
1 row in set (0.00 sec)
*/

-- SQL INJECTION EXAMPLE
-- $id = '4'
SELECT * FROM users WHERE id = 4;
-- $id = '4; SELECT user, authentication_string from mysql.user;'
SELECT * FROM users WHERE id = 4; SELECT user, authentication_string from mysql.user;
/**
+----+------------+-----------+--------------------------+---------------------+
| id | first_name | last_name | email                    | created_at          |
+----+------------+-----------+--------------------------+---------------------+
|  4 | Fredrick   | Graham    | Keyon_Wunsch27@gmail.com | 2019-09-03 08:15:46 |
+----+------------+-----------+--------------------------+---------------------+
1 row in set (0.02 sec)

+---------------+-------------------------------------------+
| user          | authentication_string                     |
+---------------+-------------------------------------------+
| mysql.sys     | *THISISNOTAVALIDPASSWORDTHATCANBEUSEDHERE |
| mysql         | *E74858DB86EBA20BC33D0AECAE8A8108C56B17FA |
| root          |                                           |
| mysql.session | *THISISNOTAVALIDPASSWORDTHATCANBEUSEDHERE |
+---------------+-------------------------------------------+
4 rows in set (0.00 sec)
*/

-- Prepaired statement
-- Prepared statemt is stored in buffer, is destroyed is session is terminated
PREPARE sql_statement FROM '
SELECT id, first_name, last_name 
FROM users 
WHERE first_name = ? AND id < ?
'; 
/**
Query OK, 0 rows affected (0.02 sec)
Statement prepared
*/

SET @first_name = 'John';
SET @id = 100000;
EXECUTE sql_statement USING @first_name, @id;

SET @first_name = 'Mike';
SET @id = '10000';
EXECUTE sql_statement USING @first_name, @id;
/**
+------+------------+-----------+
| id   | first_name | last_name |
+------+------------+-----------+
| 7518 | Mike       | Quitzon   |
+------+------------+-----------+
1 row in set (0.03 sec)
*/

SET @first_name = 'Mike';
SET @id = '10000; SELECT user, authentication_string from mysql.user;';
EXECUTE sql_statement USING @first_name, @id;
/**
+------+------------+-----------+
| id   | first_name | last_name |
+------+------------+-----------+
| 7518 | Mike       | Quitzon   |
+------+------------+-----------+
1 row in set (0.03 sec)
*/

DROP PREPARE sql_statement;

/**
IF there many similar queries with different params 
it's very efficient to prepare one statememt 
and execute it many times with different params.
*/