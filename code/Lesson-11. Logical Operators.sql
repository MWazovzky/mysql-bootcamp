-- Logical Operators
-- NOT EQUAL
SELECT * FROM books 
WHERE released_year != 2017 
ORDER BY released_year DESC;

SELECT * FROM books 
WHERE author_last_name != 'Foster Wallace';

-- NOT LIKE
SELECT * FROM books 
WHERE author_first_name LIKE '%Da%';

SELECT * FROM books 
WHERE author_first_name NOT LIKE '%Da%';

-- GREATER THEN, GREATER THEN OR EQUAL
SELECT 99 > 1; -- 1
SELECT 1 > 99; -- 0
-- String comparision
SELECT 'a' > 'b'; -- 0, 'a' is less then 'b'
SELECT 'A' > 'a'; -- 0, 
SELECT 'A' < 'a'; -- 0, 'a' is equal 'A' 

SELECT title, released_year FROM books 
WHERE released_year > 2000 
ORDER BY released_year DESC;

SELECT title, released_year FROM books 
WHERE released_year >= 2000 
ORDER BY released_year DESC;

-- LESS THEN, LESS THEN OR EQUAL
SELECT title, released_year FROM books 
WHERE released_year < 2000 
ORDER BY released_year DESC;

SELECT title, released_year FROM books 
WHERE released_year <= 2000 
ORDER BY released_year DESC;

-- AND
SELECT title, author_last_name, author_first_name, released_year FROM books 
WHERE author_last_name = 'Eggers' AND author_first_name = 'Dave' AND released_year >= 2010
ORDER BY released_year DESC;

SELECT title, author_last_name, author_first_name, released_year FROM books 
WHERE author_last_name = 'Eggers' && author_first_name = 'Dave' && released_year >= 2010
ORDER BY released_year DESC;

-- OR
SELECT title, author_first_name, author_last_name, released_year FROM books
WHERE author_last_name = 'Eggers' OR released_year >= 2015
ORDER BY released_year DESC;

SELECT title, author_first_name, author_last_name, released_year FROM books
WHERE (author_last_name = 'Eggers' OR  author_last_name = 'Carver') AND released_year >= 1980
ORDER BY released_year DESC;

-- BETWEEN
SELECT title, author_first_name, author_last_name, released_year FROM books
WHERE released_year BETWEEN 2000 AND 2010
ORDER BY released_year DESC;

SELECT title, author_first_name, author_last_name, released_year FROM books
WHERE released_year NOT BETWEEN 2000 AND 2010
ORDER BY released_year DESC;

-- BETWEEN with DATES AND TIMES
-- Recoommendation is to use CAST()
SELECT '2020-02-22';
SELECT CAST('2020-02-22' AS DATETIME);

SELECT * FROM people 
WHERE dt BETWEEN CAST('1000-01-01' AS DATETIME) AND CAST('2010-01-01' AS DATETIME);

SELECT * FROM people 
WHERE CAST(dt AS DATE) BETWEEN '1000-01-01' AND '2010-01-01';

-- IN
SELECT title, author_first_name, author_last_name FROM books
WHERE author_last_name IN ('Eggers', 'Lahiri', 'Steinbeck');

SELECT title, author_first_name, author_last_name, released_year FROM books
WHERE released_year IN (2001, 2002, 2003, 2004, 2005)
ORDER BY released_year DESC;

SELECT title, author_first_name, author_last_name, released_year FROM books
WHERE released_year NOT IN (2001, 2002, 2003, 2004, 2005)
ORDER BY released_year DESC;

-- Remainder operator (MODULO) is available in mySQL
SELECT title, author_first_name, author_last_name, released_year FROM books
WHERE released_year % 2 = 1
ORDER BY released_year DESC;

-- CASE STATEMENT
SELECT title, released_year, 
        ASDHVBNM,.ЬТМ? 
        WHEN released_year >= 2000 THEN 'Moderd literature'
        ELSE '20th century literature'
    END AS period
FROM books;

SELECT title, stock_quantity, 
    CASE 
        WHEN stock_quantity <  50 THEN '*'
        WHEN stock_quantity < 100 THEN '**'
        ELSE '***'
    END AS stock
FROM books;

-- EXERCISES
SELECT 10 != 10; -- 0
SELECT 15 > 14 && 99 - 5 <= 94; -- 1
SELECT 1 IN (5, 3) || 9 BETWEEN 8 AND 10; -- 1

SELECT title, released_year 
FROM books 
WHERE released_year < 1980
ORDER BY released_year;
/*
+-------------+---------------+
| title       | released_year |
+-------------+---------------+
| Cannery Row |          1945 |
+-------------+---------------+
1 row in set (0.05 sec)
*/

SELECT title, author_first_name, author_last_name 
FROM books 
WHERE author_last_name = 'Eggers' OR author_last_name = 'Chabon'
ORDER BY author_last_name;

SELECT title, author_first_name, author_last_name 
FROM books 
WHERE author_last_name IN ('Eggers', 'Chabon')
ORDER BY author_last_name;
/*
+-------------------------------------------+-------------------+------------------+
| title                                     | author_first_name | author_last_name |
+-------------------------------------------+-------------------+------------------+
| The Amazing Adventures of Kavalier & Clay | Michael           | Chabon           |
| A Hologram for the King: A Novel          | Dave              | Eggers           |
| The Circle                                | Dave              | Eggers           |
| A Heartbreaking Work of Staggering Genius | Dave              | Eggers           |
+-------------------------------------------+-------------------+------------------+
4 rows in set (0.00 sec)
*/

SELECT title, author_first_name, author_last_name, released_year
FROM books 
WHERE author_last_name = 'Lahiri' AND released_year > 2000
ORDER BY released_year;
/*
+--------------+-------------------+------------------+---------------+
| title        | author_first_name | author_last_name | released_year |
+--------------+-------------------+------------------+---------------+
| The Namesake | Jhumpa            | Lahiri           |          2003 |
+--------------+-------------------+------------------+---------------+
1 row in set (0.02 sec)
*/

SELECT title, pages
FROM books 
WHERE pages BETWEEN 100 AND 200
ORDER BY pages;
/*
+-----------------------------------------------------+-------+
| title                                               | pages |
+-----------------------------------------------------+-------+
| What We Talk About When We Talk About Love: Stories |   176 |
| Cannery Row                                         |   181 |
| Interpreter of Maladies                             |   198 |
+-----------------------------------------------------+-------+
3 rows in set (0.02 sec)
*/

SELECT title,  author_first_name, author_last_name
FROM books 
WHERE author_last_name LIKE 'C%' OR  author_last_name LIKE 'S%' 
ORDER BY author_last_name;
/*
+-----------------------------------------------------+-------------------+------------------+
| title                                               | author_first_name | author_last_name |
+-----------------------------------------------------+-------------------+------------------+
| What We Talk About When We Talk About Love: Stories | Raymond           | Carver           |
| Where I'm Calling From: Selected Stories            | Raymond           | Carver           |
| The Amazing Adventures of Kavalier & Clay           | Michael           | Chabon           |
| Lincoln In The Bardo                                | George            | Saunders         |
| Just Kids                                           | Patti             | Smith            |
| Cannery Row                                         | John              | Steinbeck        |
+-----------------------------------------------------+-------------------+------------------+
6 rows in set (0.05 sec)
*/

SELECT title,  author_first_name, author_last_name, 
    CASE 
        WHEN title LIKE '%stories%' THEN 'Short Stories'
        WHEN title = 'Just Kids' OR title = 'A Heartbreaking Work of Staggering Genius' THEN 'Memoir'
        ELSE 'Novel'
    END AS genre
FROM books;

SELECT title,  author_first_name, author_last_name, 
    CASE 
        WHEN title LIKE '%stories%' THEN 'Short Stories'
        WHEN title IN ('Just Kids', 'A Heartbreaking Work of Staggering Genius') THEN 'Memoir'
        ELSE 'Novel'
    END AS genre
FROM books;
/*
+-----------------------------------------------------+-------------------+------------------+---------------+
| title                                               | author_first_name | author_last_name | genre         |
+-----------------------------------------------------+-------------------+------------------+---------------+
| The Namesake                                        | Jhumpa            | Lahiri           | Novel         |
| Norse Mythology                                     | Neil              | Gaiman           | Novel         |
| American Gods                                       | Neil              | Gaiman           | Novel         |
| Interpreter of Maladies                             | Jhumpa            | Lahiri           | Novel         |
| A Hologram for the King: A Novel                    | Dave              | Eggers           | Novel         |
| The Circle                                          | Dave              | Eggers           | Novel         |
| The Amazing Adventures of Kavalier & Clay           | Michael           | Chabon           | Novel         |
| Just Kids                                           | Patti             | Smith            | Memoir        |
| A Heartbreaking Work of Staggering Genius           | Dave              | Eggers           | Memoir        |
| Coraline                                            | Neil              | Gaiman           | Novel         |
| What We Talk About When We Talk About Love: Stories | Raymond           | Carver           | Short Stories |
| Where I'm Calling From: Selected Stories            | Raymond           | Carver           | Short Stories |
| White Noise                                         | Don               | DeLillo          | Novel         |
| Cannery Row                                         | John              | Steinbeck        | Novel         |
| Oblivion: Stories                                   | David             | Foster Wallace   | Short Stories |
| Consider the Lobster                                | David             | Foster Wallace   | Novel         |
| 10% Happier                                         | Dan               | Harris           | Novel         |
| fake_book                                           | Freida            | Harris           | Novel         |
| Lincoln In The Bardo                                | George            | Saunders         | Novel         |
+-----------------------------------------------------+-------------------+------------------+---------------+
19 rows in set (0.03 sec)
*/

SELECT author_first_name, author_last_name, 
    CASE
        WHEN COUNT(*) = 1 THEN '1 book'
        ELSE CONCAT(COUNT(*), ' books')
    END AS book_count
FROM books
GROUP BY author_first_name, author_last_name
ORDER BY author_last_name;
/*
+-------------------+------------------+------------+
| author_first_name | author_last_name | book_count |
+-------------------+------------------+------------+
| Raymond           | Carver           | 2 books    |
| Michael           | Chabon           | 1 book     |
| Don               | DeLillo          | 1 book     |
| Dave              | Eggers           | 3 books    |
| David             | Foster Wallace   | 2 books    |
| Neil              | Gaiman           | 3 books    |
| Freida            | Harris           | 1 book     |
| Dan               | Harris           | 1 book     |
| Jhumpa            | Lahiri           | 2 books    |
| George            | Saunders         | 1 book     |
| Patti             | Smith            | 1 book     |
| John              | Steinbeck        | 1 book     |
+-------------------+------------------+------------+
12 rows in set (0.02 sec)
*/
