DROP DATABASE IF EXISTS app_node_db;

CREATE DATABASE app_node_db;

USE app_node_db;

CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    email VARCHAR(255) UNIQUE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO users (first_name, last_name, email) VALUES ('John', 'Dow', 'john@example.com');
