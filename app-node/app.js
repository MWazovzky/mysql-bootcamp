import express from 'express';
import routerPages from './routes/pages.js';
import routerUsers from './routes/users.js';
import error from './middleware/error.js'

const app = express();
const port = 3000;

// Handle uncaught Exceptions and Rejection
process.on('uncaughtException', (e) => { console.error({ message: e.message }) })
process.on('unhandledRejection', (e) => { throw e })

// Serve static assets
app.set('view engine', 'hbs');
app.set('views', './views')
app.use(express.static('public'))
app.use('/', routerPages)

// Serve API routes
app.use(express.json()); // parse request json && add to req.body
app.use(express.urlencoded({ extended: true })) // parse url encoded payload && add to req.body, allows to handle arrays and objects
app.use('/users', routerUsers);

// Error Handling via Middleware
app.use(error);

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});

export default app;
