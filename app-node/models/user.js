import moment from 'moment';
import faker from 'faker';
import mysql from 'mysql2';
import {promises as fs} from 'fs';

function connect() {
    return mysql.createConnection({
        host: 'localhost',
        user: 'root',
        database: 'app_node_db',
    }).promise();
}

async function query(sql, data) {
    const connection = connect();
    const result = await connection.query(sql, data);
    connection.end();
    return result;   
}

async function execute(sql, values) {
    const connection = connect();
    const result = await connection.execute({sql:sql, namedPlaceholders: true}, values)
    connection.end();
    return result;   
}

async function all() {
    const sql = 'SELECT * FROM users ORDER BY id DESC';
    const result = await query(sql);

    return result[0];
}

async function count() {
    const query = 'SELECT COUNT(*) AS count FROM users';
    const result = await execute(query);

    return result[0][0].count;
}

async function find(email) {
    const query = `SELECT * FROM users WHERE email = '${email}'`;
    const result = await execute(query);

    return result[0][0];
}

async function create({first_name, last_name, email}) {
    const sql = `INSERT INTO users (first_name, last_name, email) VALUES (:first_name, :last_name, :email)`;
    const result = await execute(sql, {first_name, last_name, email});

    return result[0];
}

async function fake() {
    const count = 500;
    const fakeUsers = generateFakeUsers(count)
    const sql = `INSERT INTO users (first_name, last_name, email, created_at) VALUES ?`;
    const result = await query(sql, [fakeUsers]);

    return result[0];
};

async function generate(count) {
    let sql = 'TRUNCATE TABLE users;';
    await query(sql);

    // Create file with users
    const filename = 'c:/users/dev/ospanel/domains/learning/mysql-bootcamp/users_data.csv';
    const content = generateFakeUsers(count)
        .map((item, index) => [index + 1, ...item].join(';'))
        .join('\r\n')
    await fs.writeFile(filename, content)

    // Load users from file into database
    sql = `LOAD DATA INFILE '${filename}' IGNORE INTO TABLE users FIELDS TERMINATED BY ';' LINES TERMINATED BY '\r\n';`;
    const result = await query(sql);

    return result;
}

/**
 * Geneate fake users
 * 
 * @param {number} count
 * @return array[array] 
 */
function generateFakeUsers(count) {
    let users = new Map();

    while (users.size != count) {
        const email = faker.internet.email();
        users.set(email, [
            faker.name.firstName(), 
            faker.name.lastName(), 
            email, 
            moment(faker.date.past()).format('YYYY-MM-DD HH:mm:ss'),
        ]);
    }
    
    return [...users.values()];
}

export default {
    all, 
    count, 
    find, 
    create, 
    fake,
    generate,
};
