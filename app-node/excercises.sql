-- EXCERCISES
-- The earliest date user was create
SELECT DATE_FORMAT(created_at, '%b %D %Y') AS earliest_date 
FROM users
ORDER BY created_at
LIMIT 1;
/**
+---------------+
| earliest_date |
+---------------+
| Dec 30th 2018 |
+---------------+
1 row in set (0.05 sec)
*/

SELECT email, created_at
FROM users
ORDER BY created_at
LIMIT 1;
/**
+------------------------+---------------------+
| email                  | created_at          |
+------------------------+---------------------+
| Dejuan.Nader@yahoo.com | 2018-12-30 13:19:58 |
+------------------------+---------------------+
1 row in set (0.03 sec)
*/

SELECT MONTHNAME(created_at) AS month, COUNT(*) AS count
FROM users
GROUP BY month
ORDER BY count DESC;
/**
+-----------+-------+
| month     | count |
+-----------+-------+
| May       |    52 |
| October   |    49 |
| September |    46 |
| December  |    45 |
| April     |    44 |
| March     |    44 |
| June      |    43 |
| November  |    41 |
| July      |    41 |
| August    |    37 |
| February  |    33 |
| January   |    25 |
+-----------+-------+
12 rows in set (0.03 sec)
*/

SELECT COUNT(*) AS yahoo_users FROM users
WHERE email LIKE '%yahoo.com'; 
/**
+-------------+
| yahoo_users |
+-------------+
|         182 |
+-------------+
1 row in set (0.02 sec)
*/

INSERT INTO users (email) VALUES ('mike@yandex.ru');

SELECT 
    CASE
        WHEN email LIKE '%gmail.com' THEN 'gmail' 
        WHEN email LIKE '%yahoo.com' THEN 'yahoo' 
        WHEN email LIKE '%hotmail.com' THEN 'hotmail' 
        ELSE 'other'
    END AS provider,
    COUNT(*) AS total_users
FROM users
GROUP BY provider
ORDER BY total_users DESC;
/**
+----------+-------------+
| provider | total_users |
+----------+-------------+
| yahoo    |         182 |
| hotmail  |         165 |
| gmail    |         153 |
| other    |           1 |
+----------+-------------+
4 rows in set (0.00 sec)
*/