const error = (err, req, res, next) => {
    if (err) {
        log(err);
        return res.status(500).send({ message: err.message })
    }

    next()
}

function log(err) {
    console.log('ERROR', {
        datetime: (new Date()),
        message: err.message,
        // stack: err.stack,
    });
}

export default error;
