import express from 'express';
import User from '../models/user.js'; 

const router = express.Router();

router.get('/',async  (req, res) => {
    try {
        const users = await User.all()
        res.json(users);
    } catch (err) {
        next(err)
    }
});

router.get('/count', async (req, res, next) => {
    try {
        const count = await User.count()
        res.json(count);    
    } catch (err) {
        next(err)
    }
});

router.get('/fake', async (req, res, next) => {
    try {
        const result = await User.fake()
        res.json(result);
    } catch (err) {
        next(err)
    }
});

router.get('/generate/:count', async (req, res, next) => {
    try {
        const result = await User.generate(req.params.count)
        res.json(result);
    } catch (err) {
        next(err)
    }
});

router.get('/:email', async (req, res, next) => {
    try {
        const user = await User.find(req.params.email)
        if (!user) return res.status(404).json({ message: 'User not found.' })
        res.json(user)
    }
    catch (err) {
        next(err)
    }
});

router.post('/', async (req, res, next) => {
    let user = await User.find(req.body.email)
    if (user) return res.status(400).send('User already exists.')

    try {
        await User.create({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
        });
        res.redirect('/home');
    }
    catch (err) {
        next(err)
    }
})

export default router;
