import express from 'express';
import User from '../models/user.js'

const router = express.Router();

router.get('/', (req, res) => {
    res.redirect('/home');
});

router.get('/home', async (req, res) => {
    const params = {
        title: 'HomePage',
        header: 'Welcome to Spam List!',
        count: await User.count(),
    }
    
    res.render('index.hbs', params);
});

export default router;
